export default function (form) {
  const data = {};
  const childrens = form.querySelectorAll('input:not([type="submit"]), select, textarea');
  childrens.forEach((element) => {
    data[element.name] = element.value;
  });
  return JSON.stringify(data);
}
