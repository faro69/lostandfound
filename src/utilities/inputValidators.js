import moment from 'moment';

export const validateRequired = value => (value ? undefined : 'Заполните это поле');
export const validateNumber = value => (value && isNaN(Number(value)) ? 'Введите число' : undefined);

export const validateDateFormat = dateFormat => date =>
  (moment(date, dateFormat, true).isValid() ? (
    undefined
  ) : (
    `Дата должна быть в формате ${dateFormat}`
  ));
