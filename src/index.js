import React from 'react';
import ReactDOM from 'react-dom';
import store from './redux';
import history from './history';
import router from './router';
import App from './components/App';

const container = document.getElementById('root');

const context = {
  store,
};

let currentLocation = history.location;

async function onLocationChange(location, action) {
  // console.log(location, action);
  try {
    const route = await router.resolve({
      ...context,
      pathname: location.pathname,
      // query: queryString.parse(location.search),
    });

    // console.log(route);

    // Prevent multiple page renders during the routing process
    if (currentLocation.key !== location.key && !action) {
      return;
    }

    if (route.redirect) {
      history.replace(route.redirect);
      return;
    }

    ReactDOM.render(<App context={context}>{route.component}</App>, container, () => {
      document.title = route.title;
    });
  } catch (error) {
    console.error(error);
    // Do a full page reload if error occurs during client-side navigation
    if (currentLocation.key === location.key) {
      window.location.reload();
    }
  }
}

let isHistoryObserverd = false;
currentLocation = history.location;
if (!isHistoryObserverd) {
  isHistoryObserverd = true;
  history.listen(onLocationChange);
}
onLocationChange(currentLocation);

