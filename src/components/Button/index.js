// @flow
import React from 'react';
import cn from 'classnames';
import history from '../../history';

type Props = {
  primary?: boolean,
  success?: boolean,
  danger?: boolean,
  warning?: boolean,
  disabled?: boolean,
  onClick?: Function,
  href?: string,
  children: any,
}

class Button extends React.Component<Props> {
  onClick = (e) => {
    if (this.props.onClick) {
      e.preventDefault();
      this.props.onClick();
    } else if (this.props.href) {
      history.push(this.props.href);
    }
  };

  render() {
    const {
      primary, success, danger, children, disabled, warning,
    } = this.props;
    const className = cn('btn', {
      'btn-primary': primary,
      'btn-success': success,
      'btn-danger': danger,
      'btn-warning': warning,
      disabled,
    });
    return (
      <button className={className} onClick={this.onClick} disabled={disabled}>{children}</button>
    );
  }
}

export default Button;
