// @flow

import React from 'react';
import { Provider } from 'react-redux';
import './style.css'

type Props = {
  context: Object,
  children: any,
}

class App extends React.PureComponent<Props> {
  render() {
    const { context, children } = this.props;
    return (
      <Provider store={context.store}>{children}</Provider>
    );
  }
}
export default App;
