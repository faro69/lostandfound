// @flow

import React from 'react';
import Button from '../Button';
import type { Journal } from '../../redux/types/journals';

type Props = {
  journal: Journal,
  deleteFunc: Function,
}

class JournalCard extends React.Component<Props> {
  deleteHandle = () => {
    if (window && window.confirm('Вы уверены, что хотите удалить этот журнал?')) { // eslint-disable-line no-undef, no-alert
      const { deleteFunc, journal: { id } } = this.props;
      deleteFunc(id);
    }
  }

  render() {
    const { journal } = this.props;
    return (
      <div className="journal">
        <h1>{journal.title_rus}/{journal.title_eng}</h1>
        <p>Аббревиатура: RUS - {journal.abbr_rus || 'Не задано'} / ENG - {journal.abbr_eng || 'Не задано'}</p>
        <p>ISSN: {journal.issn || 'Не задано'}</p>
        <p>Том: {journal.tom || 'Не задано'}</p>
        <p>Номер выпуска: {journal.number_rel || 'Не задано'}</p>
        <p>Номер части: {journal.number_chast || 'Не задано'}</p>
        <p>Назавание выпуска: {journal.release_name || 'Не задано'}</p>
        <p>Страницы: {journal.pages || 'Не задано'}</p>
        <p>Год издания: {journal.year || 'Не задано'}</p>
        <p>Дата печати:
          {journal.day_print &&
          journal.month_print &&
          journal.year_print ?
            `${journal.day_print}-${journal.month_print}-${journal.year_print}` :
            'Не задано'}
        </p>
        <div className="btn-toolbar">
          <div className="btn-group mr-2">
            <Button primary href={`/journals/${journal.id}/articles`}>Статьи</Button>
          </div>
          <div className="btn-group mr-2">
            <Button warning href={`/journals/${journal.id}/edit`}>Редактировать журнал</Button>
          </div>
          <div className="btn-group">
            <Button danger onClick={this.deleteHandle}>Удалить журнал</Button>
          </div>
        </div>
      </div>
    );
  }
}
export default JournalCard;
