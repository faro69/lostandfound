// @flow

import React from 'react';
import Button from '../Button';
import type { Author } from '../../redux/types/authors';

type Props = {
  author: Author,
  deleteFunc: Function,
}

class AuthorCard extends React.Component<Props> {
  deleteHandle = () => {
    if (window && window.confirm('Вы уверены, что хотите удалить этого автора?')) { // eslint-disable-line no-undef, no-alert
      const { deleteFunc, author: { id } } = this.props;
      deleteFunc(id);
    }
  }

  render() {
    const { author } = this.props;
    return (
      <div className="journal">
        <h3>{`${author.surname_rus} ${author.name_rus} ${author.patronymic_rus} / ${author.surname_eng} ${author.name_eng} ${author.patronymic_eng}`}</h3>
        <p>Место работы: RUS - {author.job_rus || 'Не задано'} / ENG - {author.job_eng || 'Не задано'}</p>
        <p>Город: RUS - {author.city_rus || 'Не задано'} / ENG - {author.city_eng || 'Не задано'}</p>
        <p>Адрес: RUS - {author.addr_rus || 'Не задано'} / ENG - {author.addr_eng || 'Не задано'}</p>
        <p>Другие сведения: RUS - {author.other_inform_rus || 'Не задано'} / ENG - {author.other_inform_eng || 'Не задано'}</p>
        <p>Email: {author.email || 'Не задано'}</p>
        <p>Корреспондент: {author.corresp || 'Не задано'}</p>
        <p>SPIN-код: {author.spin || 'Не задано'}</p>
        <div className="btn-toolbar">
          <div className="btn-group mr-2">
            <Button warning href={`/authors/${author.id}/edit`}>Редактировать автора</Button>
          </div>
          <div className="btn-group">
            <Button danger onClick={this.deleteHandle}>Удалить автора</Button>
          </div>
        </div>
      </div>
    );
  }
}
export default AuthorCard;
