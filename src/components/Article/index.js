// @flow

import React from 'react';
import Button from '../Button';
import type { Article } from '../../redux/types/articles';
import Link from '../Link';

type Props = {
  journalId: number,
  article: Article,
  deleteFunc: Function,
}

const multiLang = [
  { value: 'jour_section', title: 'Раздел журнала' },
  { value: 'annotation', title: 'Аннотация' },
  { value: 'keywords', title: 'Ключевые слова' },
  { value: 'financ', title: 'Финансирование' },
];


class ArticleCard extends React.Component<Props> {
  deleteHandle = () => {
    if (window && window.confirm('Вы уверены, что хотите удалить эту статью?')) { // eslint-disable-line no-undef, no-alert
      const { deleteFunc, article: { id }, journalId } = this.props;
      deleteFunc({ id, journalId });
    }
  }

  render() {
    const { article, journalId } = this.props;
    return (
      <div className="journal">
        <h1>{article.title_rus} / {article.title_eng}</h1>

        <table className="table">
          <tbody>
            {
              multiLang.map(tableRow =>
              (
                <tr key={tableRow.value}>
                  <td>{tableRow.title}</td>
                  <td>{article[`${tableRow.value}_rus`]}</td>
                  <td>{article[`${tableRow.value}_eng`]}</td>
                </tr>))
          }
          </tbody>
        </table>
        <p>Тип: {article.type || 'Не задано'}</p>
        <p>Страницы: первая {article.pages_first || 'Не задано'} / последняя - {article.pages_last || 'Не задано'}</p>
        <p>UDC: {article.udc || 'Не задано'}</p>
        <p>BBK: {article.bbk || 'Не задано'}</p>
        <p>Дата приёма: {article.receipt_date || 'Не задано'}</p>
        <p>Дата переработки: {article.recycling_date || 'Не задано'}</p>
        <p>Текст: <p>{article.text || 'Не задано'}</p></p>
        <p>DOI: {article.doi || 'Не задано'}</p>
        <p>Elibrary: {article.elibrary || 'Не задано'}</p>
        <p>Mathnet: {article.mathnet || 'Не задано'}</p>
        <p>
          <a
            href="#"
            target="_blank"
          >
            Шаблон статьи в tex
          </a>
          &nbsp;
          <Link
            href="#"
          >
            (создать шаблон заново)
          </Link>
        </p>
        <p>
          <Link
            href="#"
          >
          Создать шаблон для Elibrary
          </Link>
        </p>
        <p>
          <a
            href="#"
            target="_blank"
          >
            Шаблон статьи для Mathnet
          </a>
          &nbsp;
          <Link
            href="#"
          >
            (создать шаблон заново)
          </Link>
        </p>
        <div className="btn-toolbar">
          <div className="btn-group mr-2">
            <Button primary href={`/journals/${journalId}/articles/${article.id}/links`}>Ссылки</Button>
          </div>
          <div className="btn-group mr-2">
            <Button warning href={`/journals/${journalId}/articles/${article.id}/edit`}>Редактировать статью</Button>
          </div>
          <div className="btn-group">
            <Button danger onClick={this.deleteHandle}>Удалить статью</Button>
          </div>
        </div>
      </div>
    );
  }
}
export default ArticleCard;
