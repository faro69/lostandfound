// @flow

import React from 'react';
import Button from '../Button';
import type { Link } from '../../redux/types/links';
import { fieldLabelsByName } from '../../routes/links/linksData';

type Props = {
  journalId: number,
  articleId: number,
  link: Link,
  deleteFunc: Function,
}

class ArticleLinkCard extends React.Component<Props> {
  deleteHandle = () => {
    if (window && window.confirm('Вы уверены, что хотите удалить эту ссылку?')) { // eslint-disable-line no-undef, no-alert
      const {
        deleteFunc, link: { id }, articleId, journalId,
      } = this.props;
      deleteFunc({ id, journalId, articleId });
    }
  }

  render() {
    const { articleId, journalId, link } = this.props;
    const { title, ...otherData } = link;
    return (
      <div className="journal">
        <h1>{title}</h1>
        {
          Object.keys(otherData).map((key) => {
            if (key && otherData[key] && fieldLabelsByName[key]) {
              return <p key={key}>{fieldLabelsByName[key]}: {otherData[key]}</p>;
            }
            return null;
          })
        }
        <div className="btn-toolbar">
          <div className="btn-group mr-2">
            <Button
              warning
              href={`/journals/${journalId}/articles/${articleId}/links/${link.id}/edit`}
            >
              Редактировать ссылку
            </Button>
          </div>
          <div className="btn-group">
            <Button danger onClick={this.deleteHandle}>Удалить ссылку</Button>
          </div>
        </div>
      </div>
    );
  }
}
export default ArticleLinkCard;
