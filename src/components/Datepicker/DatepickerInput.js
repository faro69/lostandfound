// @flow
import React from 'react';
import Datepicker from 'react-datetime';
import moment from 'moment';
import cn from 'classnames';
import 'react-datetime/css/react-datetime.css';

type Props = {
  label: string,
  name: string,
  required?: boolean,
  dateFormat?: string,
  input: Object,
  meta: Object,
};

class DatepickerInput extends React.Component<Props> {
  static defaultProps = {
    dateFormat: 'DD-MM-YYYY',
  }

  handleChange = (date) => {
    const { input: { onChange }, dateFormat } = this.props;
    if (typeof date !== 'string' && moment.isMoment(date)) {
      onChange(date.format(dateFormat));
    } else {
      onChange(date);
    }
  }

  renderInput = (inputProps, openCalendar) => {
    const {
      label, required, name, input, meta: { touched, error },
    } = this.props;
    const inputClass = cn('form-control', { 'is-valid': touched && !error, 'is-invalid': touched && error });

    return (
      <div className="form-group">
        <label htmlFor={name} className="form-control-label">
          {label}
          {!required && ' (необязательно)'}
        </label>
        <input
          {...input}

          id={name}
          className={inputClass}
          placeholder="Нажмите для выбора даты"
          onClick={openCalendar}
        />
        {touched && error && <div className="invalid-feedback">{error}</div>}
      </div>
    );
  };

  render() {
    const { input, dateFormat } = this.props;
    return (<Datepicker
      value={input.value}
      inputProps={{ placeholder: 'Нажмите для выбора даты', disabled: true }}
      onChange={this.handleChange}
      renderInput={this.renderInput}
      dateFormat={dateFormat}
      timeFormat={false}
    />);
  }
}

export default DatepickerInput;
