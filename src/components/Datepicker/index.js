import React from 'react';
import { Field } from 'redux-form';
import { validateRequired } from '../../utilities/inputValidators';
import DatepickerInput from './DatepickerInput';

type Props = {
  label: string,
  name: string,
  required?: boolean,
  dateFormat?: string,
};

class Datepicker extends React.Component<Props> {
  static defaultProps = {
    dateFormat: 'DD-MM-YYYY',
  }

  renderComponent = inputProps => <DatepickerInput {...inputProps} {...this.props} />;

  render() {
    const { name, required } = this.props;
    const validators = [];

    if (required) {
      validators.push(validateRequired);
    }

    return <Field name={name} component={this.renderComponent} validate={validators} />;
  }
}

export default Datepicker;
