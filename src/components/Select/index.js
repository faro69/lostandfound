/* eslint-disable jsx-a11y/label-has-for */
// @flow
import React from 'react';
import { Field } from 'redux-form';
import cn from 'classnames';
import ReactSelect from 'react-select';
import 'react-select/dist/react-select.css';
import { validateRequired } from '../../utilities/inputValidators';
import type { Author } from '../../redux/types/authors';

type Props = {
  name: string,
  label: string,
  required?: boolean;
  options: Array<{value: any, label: any}>,
  multi?: boolean,
  clearable?: boolean,
}

class Select extends React.Component<Props> {

  static defaultProps = {
    clearable: false,
  }

  onChange = (handler: Function) => (data: {value: any, label: any}) => {
    if (data) {
      if (Array.isArray(data)) {
        handler(data.map(d => d.value));
      } else {
        handler(data.value);
      }
    } else {
      handler(null);
    }
  };

  renderComponent = ({ input, meta: { touched, error } }) => {
    const {
      label, name, required, options, multi, clearable,
    } = this.props;

    const inputClass = cn('form-control', { 'is-valid': touched && !error, 'is-invalid': touched && error });
    return (
      <div className="form-group">
        <label htmlFor={name} className="form-control-label">
          {label}
          {!required && ' (необязательно)'}
        </label>
        <ReactSelect
          name={name}
          options={options}
          onChange={this.onChange(input.onChange)}
          multi={multi}
          clearable={clearable}
          value={input.value}
          placeholder="Выберите..."
        />
        {touched && error && <div className="invalid-feedback">{error}</div>}
      </div>
    );
  }

  render() {
    const { name, required } = this.props;
    const validators = [];
    if (required) {
      validators.push(validateRequired);
    }
    return <Field name={name} validate={validators} component={this.renderComponent} />;
  }
}

export default Select;
