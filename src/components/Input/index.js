// @flow

import React from 'react';
import cn from 'classnames';
import { Field } from 'redux-form';
import { validateNumber, validateRequired } from '../../utilities/inputValidators';

type Props = {
  label: string,
  name: string,
  required?: boolean,
  isNumber?: boolean,
  multiline?: boolean,
};

class Input extends React.Component<Props> {
  renderComponent = ({ input, meta: { error, touched } }) => {
    const {
      label, name, required, multiline,
    } = this.props;

    const inputClass = cn('form-control', { 'is-valid': touched && !error, 'is-invalid': touched && error });
    return (
      <div className="form-group">
        <label htmlFor={name} className="form-control-label">
          {label}
          {!required && ' (необязательно)'}
        </label>
        {
          multiline ? <textarea {...input} id={name} className={inputClass} />
            : <input {...input} id={name} className={inputClass} />
        }
        {touched && error && <div className="invalid-feedback">{error}</div>}
      </div>
    );
  };

  render() {
    const {
      name, required, isNumber, ...rest
    } = this.props;
    const validators = [];
    if (required) {
      validators.push(validateRequired);
    }
    if (isNumber) {
      validators.push(validateNumber);
    }
    return (
      <Field name={name} component={this.renderComponent} validate={validators} {...rest} />
    );
  }
}

export default Input;
