// @flow

import React from 'react';
import PropTypes from 'prop-types';
import history from '../../history';

function isLeftClickEvent(event) {
  return event.button === 0;
}

function isModifiedEvent(event) {
  return !!(event.metaKey || event.altKey || event.ctrlKey || event.shiftKey);
}

class Link extends React.Component {
  static propTypes = {
    href: PropTypes.string,
    children: PropTypes.node.isRequired,
    onClick: PropTypes.func,
    download: PropTypes.string,
  };

  static defaultProps = {
    href: null,
    onClick: null,
    download: null,
  };

  handleClick = (event) => {
    if (this.props.onClick) {
      this.props.onClick(event);
    }

    if (isModifiedEvent(event) || !isLeftClickEvent(event)) {
      return;
    }

    if (event.defaultPrevented === true) {
      return;
    }

    event.stopPropagation();

    if (this.props.download || !this.props.href) {
      return;
    }

    event.preventDefault();
    if (this.props.href && this.props.href.includes('?')) {
      history.push(this.props.href);
    } else {
      history.push({ pathname: this.props.href, search: history.location.search });
    }
  };

  render() {
    const { href, children, ...props } = this.props;
    return React.createElement(
      href ? 'a' : 'div',
      {
        ...props,
        href,
        onClick: this.handleClick,
      },
      children,
    );
  }
}

export default Link;
