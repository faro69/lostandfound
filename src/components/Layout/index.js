// @flow
import React from 'react';
import Link from '../Link';
import history from '../../history';

type Props = {
  children: any,
  breadcrumbs: Array<{
    href: string,
    label: string,
  }>
}

export default ({ children, breadcrumbs = [] }: Props) =>
  (
    <div className="container-fluid">
      <div className="navbar fixed-top navbar-dark bg-primary">
        <div className="container">
          <Link href="/" className="navbar-brand">Главная страница</Link>
        </div>
      </div>
      <div
        className="container"
        style={{
        paddingTop: '100px',
        paddingBottom: '50px',
      }}
      >
        {
          !!breadcrumbs.length && (
            <nav aria-label="breadcrumb">
              <ol className="breadcrumb">
                {
                  breadcrumbs.map(({ label, href }) => (
                    <li
                      className={href ? 'breadcrumb-item active' : 'breadcrumb-item'}
                      key={`${label}-${href}`}
                    >
                      {
                        href ? <Link href={href}>{label}</Link> : label
                      }
                    </li>))
                }
              </ol>
            </nav>
          )
        }
        {children}
      </div>
    </div>
  );
