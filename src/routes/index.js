import { loadRoot, loadChild } from './actions';

const routes = {
  path: '',
  action: loadRoot,
  children: [
    {
      path: '/',
      action: loadChild,
      children: [
        {
          path: '',
          load: () => import('./main'),
        },
        {
          path: '/journals',
          action: loadChild,
          children: [
            {
              path: '',
              load: () => import('./journals'),
            },
            {
              path: '/add',
              load: () => import('./journals/add'),
            },
            {
              path: '/:journalId',
              action: loadChild,
              children: [
                {
                  path: '',
                  load: () => import('./journals/journal'),
                },
                {
                  path: '/edit',
                  load: () => import('./journals/edit'),
                },
                {
                  path: '/articles',
                  action: loadChild,
                  children: [
                    {
                      path: '',
                      load: () => import('./articles'),
                    },
                    {
                      path: '/add',
                      load: () => import('./articles/add'),
                    },
                    {
                      path: '/:articleId',
                      action: loadChild,
                      children: [
                        {
                          path: '',
                          load: () => import('./articles/article'),
                        },
                        {
                          path: '/edit',
                          load: () => import('./articles/edit'),
                        },
                        {
                          path: '/links',
                          action: loadChild,
                          children: [
                            {
                              path: '',
                              load: () => import('./links'),
                            },
                            {
                              path: '/add',
                              load: () => import('./links/add'),
                            },
                            {
                              path: '/:linkId',
                              action: loadChild,
                              children: [
                                {
                                  path: '',
                                  load: () => import('./links/link'),
                                },
                                {
                                  path: '/edit',
                                  load: () => import('./links/edit'),
                                },
                              ],
                            },
                          ],
                        },
                      ],
                    },
                  ],
                },
              ],
            },
          ],
        },
        {
          path: '/authors',
          action: loadChild,
          children: [
            {
              path: '',
              load: () => import('./authors'),
            },
            {
              path: '/add',
              load: () => import('./authors/add'),
            },
            {
              path: '/:authorId',
              action: loadChild,
              children: [
                {
                  path: '',
                  load: () => import('./authors/author'),
                },
                {
                  path: '/edit',
                  load: () => import('./authors/edit'),
                },
              ],
            },
          ],
        },
      ],
    },
    { path: '(.*)', load: () => import('./not-found') },
  ],
};

export default routes;
