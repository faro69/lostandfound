import React from 'react';
import Layout from '../../components/Layout';

async function action() {
  return {
    title: '404',
    component: (
      <Layout>
        <div>
          <b>Страница не найдена</b>
        </div>
      </Layout>

    ),
  };
}

export default action;
