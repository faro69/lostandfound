import React from 'react';
import isEmpty from 'just-is-empty';
import Layout from '../../components/Layout';
import { fetchJournal } from '../../redux/modules/journals';
import { fetchJournalArticles } from '../../redux/modules/articles';
import Articles from './Articles';

async function action({ store: { getState, dispatch }, params: { journalId } }) {
  const { articles } = getState();
  let { journals } = getState();

  await Promise.all([
    isEmpty(journals.byId[journalId]) && dispatch(fetchJournal(journalId)),
    isEmpty(articles.byJournal[journalId]) && dispatch(fetchJournalArticles(journalId)),
  ]);
  ({ journals } = getState());
  const journal = journals.byId[journalId];
  if (isEmpty(journal)) { return { redirect: '/404' }; }

  const breadcrumbs = [
    { label: 'Журналы', href: '/journals' },
    { label: `${journal.title_rus} / ${journal.title_eng}`, href: `/journals/${journalId}` },
    { label: 'Статьи' },
  ];

  return {
    title: 'Статьи',
    component: (
      <Layout breadcrumbs={breadcrumbs}>
        <Articles journalId={journalId} />
      </Layout>

    ),
  };
}

export default action;
