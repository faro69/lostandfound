// @flow

import React from 'react';
import { connect } from 'react-redux';
import Link from '../../components/Link';
import Button from '../../components/Button';
import type { Article } from '../../redux/types/articles';

type Props = {
  articles: Array<Article>,
  journalId: number,
}

class Articles extends React.Component<Props> {
  render() {
    const { articles = [], journalId } = this.props;
    return (
      <div className="journals">
        {
          !articles.length && <h3>Нет ни одной статьи.</h3>
        }
        <p>
          <Button primary href={`/journals/${journalId}/articles/add`}>Добавить статью</Button>
        </p>
        <ul className="list-group">
          {
            articles.map(article =>
              (
                <li className="list-group-item d-flex justify-content-between align-items-center" key={article.id}>
                  <Link
                    href={`/journals/${journalId}/articles/${article.id}`}
                  >
                    {article.title_rus} / {article.title_eng}
                  </Link>
                </li>
              ))
          }
        </ul>
      </div>
    );
  }
}

const mapState = ({ articles }, { journalId }) => ({ articles: articles.byJournal[journalId] });
export default connect(mapState)(Articles);
