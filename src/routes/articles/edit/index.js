import React from 'react';
import isEmpty from 'just-is-empty';
import Layout from '../../../components/Layout';
import { fetchJournal } from '../../../redux/modules/journals';
import { fetchArticle } from '../../../redux/modules/articles';
import EditArticle from './EditArticle';
import { fetchAuthors } from '../../../redux/modules/authors';

async function action({ store: { getState, dispatch }, params: { journalId, articleId } }) {
  let { journals, articles } = getState();
  const { authors } = getState();

  await Promise.all([
    isEmpty(journals.byId[journalId]) && dispatch(fetchJournal(journalId)),
    isEmpty(articles.byId[articleId]) && dispatch(fetchArticle(articleId)),
    isEmpty(authors.items) && dispatch(fetchAuthors()),
  ]);
  ({ journals, articles } = getState());
  const journal = journals.byId[journalId];
  const article = articles.byId[articleId];
  if (isEmpty(journal) || isEmpty(article) || article.journal_id !== journal.id) { return { redirect: '/404' }; }

  const breadcrumbs = [
    { label: 'Журналы', href: '/journals' },
    { label: `${journal.title_rus} / ${journal.title_eng}`, href: `/journals/${journalId}` },
    { label: 'Статьи', href: `/journals/${journalId}/articles` },
    {
      label: `${article.title_rus} / ${article.title_eng}`,
      href: `/journals/${journalId}/articles/${articleId}`,
    },
    { label: 'Редактировать статью' },
  ];

  return {
    title: `Редактирование статьи ${article.title_ru} / ${article.title_eng}`,
    component: (
      <Layout breadcrumbs={breadcrumbs}>
        <EditArticle articleId={articleId} journalId={journalId} />
      </Layout>

    ),
  };
}

export default action;
