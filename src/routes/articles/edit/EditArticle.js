// @flow
import React from 'react';
import { connect } from 'react-redux';
import { updateArticle } from '../../../redux/modules/articles';
import ArticleForm from '../ArticleForm';
import type { Article } from '../../../redux/types/articles';

type Props = {
  journalId: number,
  article: Article,
  updateArticle: Function,
};

class EditArticle extends React.Component<Props> {
  onSubmit = (data) => {
    this.props.updateArticle({ ...data, journalId: this.props.journalId });
  }

  render() {
    const { article, journalId } = this.props;
    const formSettings = {
      form: 'editArticle',
      onSubmit: this.onSubmit,
      initialValues: {
        ...article,
        authors: article.authors && article.authors.map(({ id }) => id),
      },
      closeHref: `/journals/${journalId}/articles/${article.id}`,
    };
    return (
      <ArticleForm {...formSettings} edit />
    );
  }
}

const mapState = ({ articles }, { articleId }) => ({ article: articles.byId[articleId] });
const mapDispatch = { updateArticle };
export default connect(mapState, mapDispatch)(EditArticle);
