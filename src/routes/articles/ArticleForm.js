// @flow
import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { reduxForm } from 'redux-form';
import Input from '../../components/Input/index';
import Button from '../../components/Button/index';
import Datepicker from '../../components/Datepicker';
import type { AuthorsState } from '../../redux/types/authors';
import Select from '../../components/Select';


type Props = {
  handleSubmit: Function,
  edit: boolean,
  authors: AuthorsState,
  closeHref: string,
}

const articlesMultilangFields = [
  { name: 'title', label: 'Название статьи', required: true },
  { name: 'jour_section', label: 'Раздел журнала', required: true },
  { name: 'annotation', label: 'Аннотация', required: true },
  { name: 'keywords', label: 'Ключевые слова', required: true },
  { name: 'financ', label: 'Финансирование', required: true },

];

class ArticleForm extends React.Component<Props> {
  render() {
    const {
      handleSubmit, edit, authors: { items: authorsItems }, closeHref,
    } = this.props;
    const authorsOptions = authorsItems.length ?
      authorsItems.map(author => ({ label: `${author.surname_rus} ${author.name_rus} ${author.patronymic_rus} / ${author.surname_eng} ${author.name_eng} ${author.patronymic_eng}`, value: author.id })) :
      [];
    return (
      <form onSubmit={handleSubmit}>
        <h1>{edit ? 'Изменить' : 'Создать'} статью</h1>
        <div className="row">
          {
            articlesMultilangFields.map(langField =>
              (
                <Fragment key={langField.name}>
                  <div className="col-md-6">
                    <Input name={`${langField.name}_rus`} label={langField.label} required={langField.required} />
                  </div>
                  <div className="col-md-6">
                    <Input name={`${langField.name}_eng`} label={`${langField.label} на английском`} required={langField.required} />
                  </div>
                </Fragment>
              ))
          }
          <div className="col-lg-6">
            <Select name="authors" label="Авторы" options={authorsOptions} multi required />
          </div>
          <div className="col-lg-6">
            <Input name="type" label="Тип статьи" required isNumber />
          </div>
          <div className="col-lg-6">
            <Input name="pages_first" label="Первая страница" required isNumber />
          </div>
          <div className="col-lg-6">
            <Input name="pages_last" label="Последняя страница" required isNumber />
          </div>
          <div className="col-lg-6">
            <Input name="udc" label="UDC" required />
          </div>
          <div className="col-lg-6">
            <Input name="bbk" label="BBK" required />
          </div>
          <div className="col-lg-6">
            <Datepicker name="receipt_date" label="Дата поступления" required />
          </div>
          <div className="col-lg-6">
            <Datepicker name="recycling_date" label="Дата переработки" required />
          </div>
          <div className="col-lg-6">
            <Input name="text" label="Текст" required multiline />
          </div>
          <div className="col-lg-6">
            <Input name="doi" label="DOI" required />
          </div>
          <div className="col-lg-6">
            <Input name="elibrary" label="elibrary" required />
          </div>
        </div>
        <div className="btn-toolbar">
          <div className="btn-group mr-2">
            <Button type="submit" success>
              {edit ? 'Изменить' : 'Создать'} статью
            </Button>
          </div>
          <div className="btn-group">
            <Button danger href={closeHref}>Закрыть</Button>
          </div>
        </div>
      </form>
    );
  }
}

const mapState = ({ authors }) => ({ authors });
export default compose(reduxForm(), connect(mapState))(ArticleForm);
