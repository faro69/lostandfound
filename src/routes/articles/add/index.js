import React from 'react';
import isEmpty from 'just-is-empty';
import AddArticle from './AddArticle';
import { fetchJournal } from '../../../redux/modules/journals';
import Layout from '../../../components/Layout';
import { fetchAuthors } from '../../../redux/modules/authors';


async function action({ store: { getState, dispatch }, params: { journalId } }) {
  let { journals } = getState();
  const { authors } = getState();

  await Promise.all([
    isEmpty(journals.byId[journalId]) && dispatch(fetchJournal(journalId)),
    isEmpty(authors.items) && dispatch(fetchAuthors()),
  ]);

  ({ journals } = getState());
  const journal = journals.byId[journalId];
  if (isEmpty(journal)) { return { redirect: '/404' }; }

  const breadcrumbs = [
    { label: 'Журналы', href: '/journals' },
    { label: `${journal.title_rus} / ${journal.title_eng}`, href: `/journals/${journalId}` },
    { label: 'Добавить статью' },
  ];
  return {
    title: 'Добавить статью',
    component: (
      <Layout breadcrumbs={breadcrumbs}>
        <AddArticle journalId={journalId} />
      </Layout>

    ),
  };
}

export default action;
