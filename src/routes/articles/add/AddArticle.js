// @flow
import React from 'react';
import { connect } from 'react-redux';
import ArticleForm from '../ArticleForm';
import { createArticle } from '../../../redux/modules/articles';

type Props = {
  createArticle: Function,
  journalId: number,
};

class AddArticle extends React.Component<Props> {
  onSubmit = (data) => {
    this.props.createArticle({ data, journalId: this.props.journalId });
  }

  render() {
    const { journalId } = this.props;
    const formSettings = {
      form: 'addArticle',
      onSubmit: this.onSubmit,
      initialValues: {
        journal_id: journalId,
      },
      closeHref: `/journals/${journalId}/articles`,
    };
    return (
      <ArticleForm {...formSettings} />
    );
  }
}

const mapDispatch = { createArticle };
export default connect(null, mapDispatch)(AddArticle);
