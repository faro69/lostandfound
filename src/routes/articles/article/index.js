import React from 'react';
import isEmpty from 'just-is-empty';
import Layout from '../../../components/Layout';
import { fetchJournal } from '../../../redux/modules/journals';
import { fetchArticle } from '../../../redux/modules/articles';
import Article from './Article';

async function action({ store: { getState, dispatch }, params: { journalId, articleId } }) {
  let { journals, articles } = getState();

  await Promise.all([
    isEmpty(journals.byId[journalId]) && dispatch(fetchJournal(journalId)),
    isEmpty(articles.byId[articleId]) && dispatch(fetchArticle(articleId)),
  ]);
  ({ journals, articles } = getState());
  const journal = journals.byId[journalId];
  const article = articles.byId[articleId];
  if (isEmpty(journal) || isEmpty(article) || article.journal_id !== journal.id) { return { redirect: '/404' }; }

  const breadcrumbs = [
    { label: 'Журналы', href: '/journals' },
    { label: `${journal.title_rus} / ${journal.title_eng}`, href: `/journals/${journalId}` },
    { label: 'Статьи', href: `/journals/${journalId}/articles` },
    { label: `${article.title_rus} / ${article.title_eng}` },
  ];

  return {
    title: `${article.title_rus} / ${article.title_eng}`,
    component: (
      <Layout breadcrumbs={breadcrumbs}>
        <Article articleId={articleId} journalId={journalId} />
      </Layout>

    ),
  };
}

export default action;
