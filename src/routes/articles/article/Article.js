// @flow

import React from 'react';
import { connect } from 'react-redux';
import ArticleCard from '../../../components/Article';
import { deleteArticle } from '../../../redux/modules/articles';
import type { Article as ArticleType } from '../../../redux/types/articles';

type Props = {
  journalId: number,
  article: ArticleType,
  deleteFunc: Function,
}

class Article extends React.Component<Props> {
  render() {
    const { article, deleteFunc, journalId } = this.props;
    return (
      <ArticleCard article={article} deleteFunc={deleteFunc} journalId={journalId} />
    );
  }
}

const mapState = ({ articles }, { articleId }) => ({ article: articles.byId[articleId] });
export default connect(mapState, { deleteFunc: deleteArticle })(Article);
