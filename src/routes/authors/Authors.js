// @flow

import React from 'react';
import { connect } from 'react-redux';
import Link from '../../components/Link';
import Button from '../../components/Button';
import type { AuthorsState } from '../../redux/types/authors';

type Props = {
  authors: AuthorsState,
  loading: boolean,
}

class Authors extends React.Component<Props> {
  render() {
    const { authors: { items }, loading } = this.props;
    return (
      <div className="journals">
        {
          !items.length && <h3>Не добавлено ни одного автора.</h3>
        }
        <p>
          <Button primary href="/authors/add">Добавить автора</Button>
        </p>
        <ul className="list-group">
          {
            loading ? <p>Загрузка...</p> :
              items.map(author =>
                (
                  <li className="list-group-item d-flex justify-content-between align-items-center" key={author.id}>
                    <Link
                      href={`/authors/${author.id}`}
                    >
                      {`${author.surname_rus} ${author.name_rus} ${author.patronymic_rus} / ${author.surname_eng} ${author.name_eng} ${author.patronymic_eng}`}
                    </Link>
                  </li>
                ))
          }
        </ul>
      </div>
    );
  }
}

const mapState = ({ authors }) => ({ authors, loading: authors.loading });
export default connect(mapState)(Authors);
