import React from 'react';
import Layout from '../../../components/Layout';
import AddAuthor from './AddAuthor';

async function action() {
  const breadcrumbs = [
    { label: 'Авторы', href: '/authors' },
    { label: 'Добавить автора' },
  ];

  return {
    title: 'Добавить автора',
    component: (
      <Layout breadcrumbs={breadcrumbs}>
        <AddAuthor />
      </Layout>
    ),
  };
}

export default action;
