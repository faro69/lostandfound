// @flow
import React from 'react';
import { connect } from 'react-redux';
import { createAuthor } from '../../../redux/modules/authors';
import AuthorForm from '../AuthorForm';

type Props = {
  createAuthor: Function,
};

class AddAuthor extends React.Component<Props> {
  onSubmit = (data) => {
    this.props.createAuthor(data);
  }
  render() {
    const formSettings = {
      form: 'addAuthor',
      onSubmit: this.onSubmit,
      initialValues: {
      },
      closeHref: '/authors',
    };
    return (
      <AuthorForm {...formSettings} />
    );
  }
}

const mapDispatch = { createAuthor };
export default connect(null, mapDispatch)(AddAuthor);
