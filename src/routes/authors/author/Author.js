// @flow

import React from 'react';
import { connect } from 'react-redux';
import { deleteAuthor } from '../../../redux/modules/authors';
import type { Author as AuthorType } from '../../../redux/types/authors';
import AuthorCard from '../../../components/Author';

type Props = {
  author: AuthorType,
  deleteFunc: Function,
}

class Author extends React.Component<Props> {
  render() {
    const { author, deleteFunc } = this.props;
    return (
      <AuthorCard author={author} deleteFunc={deleteFunc} />
    );
  }
}

const mapState = ({ authors }, { authorId }) => ({ author: authors.byId[authorId] });
export default connect(mapState, { deleteFunc: deleteAuthor })(Author);
