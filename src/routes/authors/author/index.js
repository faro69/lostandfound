import React from 'react';
import isEmpty from 'just-is-empty';
import Layout from '../../../components/Layout';
import Author from './Author';
import { fetchAuthor } from '../../../redux/modules/authors';

async function action({ store: { getState, dispatch }, params: { authorId } }) {
  let { authors } = getState();

  await Promise.all([
    isEmpty(authors.byId[authorId]) && dispatch(fetchAuthor(authorId)),
  ]);

  ({ authors } = getState());
  const author = authors.byId[authorId];
  if (isEmpty(author)) { return { redirect: '/404' }; }

  const breadcrumbs = [
    { label: 'Авторы', href: '/authors' },
    {
      label: `${author.surname_rus} ${author.name_rus} ${author.patronymic_rus} 
    / ${author.surname_eng} ${author.name_eng} ${author.patronymic_eng}`,
    },
  ];

  return {
    title: `${author.surname_rus} ${author.name_rus} ${author.patronymic_rus} 
    / ${author.surname_eng} ${author.name_eng} ${author.patronymic_eng}`,
    component: (
      <Layout breadcrumbs={breadcrumbs}>
        <Author authorId={authorId} />
      </Layout>
    ),
  };
}

export default action;
