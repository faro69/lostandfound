// @flow
import React, { Fragment } from 'react';
import { reduxForm } from 'redux-form';
import Input from '../../components/Input/index';
import Button from '../../components/Button/index';

type Props = {
  handleSubmit: Function,
  edit: boolean,
  closeHref: string,
}

const articlesMultilangFields = [
  { name: 'surname', label: 'Фамилия', required: true },
  { name: 'name', label: 'Имя', required: true },
  { name: 'patronymic', label: 'Отчество', required: true },
  { name: 'job', label: 'Место работы', required: true },
  { name: 'city', label: 'Город', required: true },
  { name: 'addr', label: 'Адрес', required: true },
  { name: 'other_inform', label: 'Другая информация', required: true },
];

class AuthorForm extends React.Component<Props> {
  render() {
    const { handleSubmit, edit, closeHref } = this.props;
    return (
      <form onSubmit={handleSubmit}>
        <h1>{edit ? 'Изменить' : 'Добавить'} автора</h1>
        <div className="row">
          {
            articlesMultilangFields.map(langField =>
              (
                <Fragment key={langField.name}>
                  <div className="col-md-6">
                    <Input name={`${langField.name}_rus`} label={langField.label} required={langField.required} />
                  </div>
                  <div className="col-md-6">
                    <Input name={`${langField.name}_eng`} label={`${langField.label} на английском`} required={langField.required} />
                  </div>
                </Fragment>
              ))
          }
          <div className="col-lg-6">
            <Input name="email" label="Email" required />
          </div>
          <div className="col-lg-6">
            <Input name="corresp" label="Корреспондент" required />
          </div>
          <div className="col-lg-6">
            <Input name="spin" label="SPIN-код" required />
          </div>
        </div>
        <div className="btn-toolbar">
          <div className="btn-group mr-2">
            <Button type="submit" success>
              {edit ? 'Изменить' : 'Добавить'} автора
            </Button>
          </div>
          <div className="btn-group">
            <Button danger href={closeHref}>Закрыть</Button>
          </div>
        </div>
      </form>
    );
  }
}

export default reduxForm()(AuthorForm);
