import React from 'react';
import isEmpty from 'just-is-empty';
import Layout from '../../components/Layout';
import { fetchAuthors } from '../../redux/modules/authors';
import Authors from './Authors';

async function action({ store: { getState, dispatch } }) {
  const { authors } = getState();
  await Promise.all([
    isEmpty(authors.items) && dispatch(fetchAuthors()),
  ]);
  return {
    title: 'Авторы',
    component: (
      <Layout closeHref="/">
        <Authors />
      </Layout>

    ),
  };
}

export default action;
