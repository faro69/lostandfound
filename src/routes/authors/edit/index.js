import React from 'react';
import isEmpty from 'just-is-empty';
import Layout from '../../../components/Layout';
import { fetchAuthor } from '../../../redux/modules/authors';
import EditAuthor from './EditAuthor';

async function action({ store: { getState, dispatch }, params: { authorId } }) {
  let { authors } = getState();

  await Promise.all([
    isEmpty(authors.byId[authorId]) && dispatch(fetchAuthor(authorId)),
  ]);

  ({ authors } = getState());
  const author = authors.byId[authorId];
  if (isEmpty(author)) { return { redirect: '/404' }; }

  const breadcrumbs = [
    { label: 'Авторы', href: '/authors' },
    {
      label: `${author.surname_rus} ${author.name_rus} ${author.patronymic_rus} 
    / ${author.surname_eng} ${author.name_eng} ${author.patronymic_eng}`,
      href: `/authors/${authorId}`,
    },
    { label: 'Редактировать автора' },
  ];

  return {
    title: `${author.surname_rus} ${author.name_rus} ${author.patronymic_rus} 
    / ${author.surname_eng} ${author.name_eng} ${author.patronymic_eng}`,
    component: (
      <Layout breadcrumbs={breadcrumbs}>
        <EditAuthor authorId={authorId} />
      </Layout>
    ),
  };
}

export default action;
