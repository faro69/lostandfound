// @flow
import React from 'react';
import { connect } from 'react-redux';
import { updateAuthor } from '../../../redux/modules/authors';
import type { Author } from '../../../redux/types/authors';
import AuthorForm from '../AuthorForm';

type Props = {
  authorId: number,
  author: Author,
  updateAuthor: Function,
};

class EditAuthor extends React.Component<Props> {
  onSubmit = (data) => {
    this.props.updateAuthor(data);
  }

  render() {
    const { author } = this.props;
    const formSettings = {
      form: 'editAuthor',
      onSubmit: this.onSubmit,
      initialValues: {
        ...author,
      },
      closeHref: `/authors/${author.id}`,
    };
    return (
      <AuthorForm {...formSettings} edit />
    );
  }
}

const mapState = ({ authors }, { authorId }) => ({ author: authors.byId[authorId] });
const mapDispatch = { updateAuthor };
export default connect(mapState, mapDispatch)(EditAuthor);
