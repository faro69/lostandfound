// export const isUserAuthenticated = store => {
//     const { user } = store.getState();
//     return Boolean(user.apiKey);
// };

export const loadRoot = async ({ next }) => {
  // Execute each child route until one of them return the result
  const route = await next();

  // Provide default values for title, description etc.
  route.title = `${route.title || 'Untitled Page'}`;
  route.description = route.description || '';
  return route;
};

export const loadChild = async ({ next }) => next();

// export const authenticateUser = async ({ next, store }) => {
//     if (!isUserAuthenticated(store)) {
//         return {
//             redirect: '/login',
//         };
//     }
//     return next();
// };

// export const redirect = url => async ({ baseUrl }) => ({ redirect: `${baseUrl}/${url}` });
