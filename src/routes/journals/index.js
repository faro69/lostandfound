import React from 'react';
import isEmpty from 'just-is-empty';
import { fetchJournals } from '../../redux/modules/journals';
import Layout from '../../components/Layout';
import Journals from './Journals';

async function action({ store: { getState, dispatch } }) {
  const { journals } = getState();
  await Promise.all([
    isEmpty(journals.items) && dispatch(fetchJournals()),
  ]);

  return {
    title: 'Журналы',
    component: (
      <Layout >
        <Journals />
      </Layout>

    ),
  };
}

export default action;
