// @flow

import React from 'react';
import { connect } from 'react-redux';
import { deleteJournal } from '../../../redux/modules/journals';
import JournalCard from '../../../components/Journal';
import type { Journal as JournalType } from '../../../redux/types/journals';

type Props = {
  journal: JournalType,
  deleteFunc: Function,
}

class Journal extends React.Component<Props> {
  render() {
    const { journal, deleteFunc } = this.props;
    return (
      <JournalCard journal={journal} deleteFunc={deleteFunc} />
    );
  }
}

const mapState = ({ journals }, { journalId }) => ({ journal: journals.byId[journalId] });
export default connect(mapState, { deleteFunc: deleteJournal })(Journal);
