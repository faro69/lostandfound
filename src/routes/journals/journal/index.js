import React from 'react';
import isEmpty from 'just-is-empty';
import Layout from '../../../components/Layout';
import { fetchJournal } from '../../../redux/modules/journals';
import Journal from './Journal';

async function action({ store: { getState, dispatch }, params: { journalId } }) {
  let { journals } = getState();

  await Promise.all([
    isEmpty(journals.byId[journalId]) && dispatch(fetchJournal(journalId)),
  ]);

  ({ journals } = getState());
  const journal = journals.byId[journalId];
  if (isEmpty(journal)) { return { redirect: '/404' }; }

  const breadcrumbs = [
    { label: 'Журналы', href: '/journals' },
    { label: `${journal.title_rus} / ${journal.title_eng}` },
  ];

  return {
    title: `${journal.title_rus} / ${journal.title_eng}`,
    component: (
      <Layout breadcrumbs={breadcrumbs}>
        <Journal journalId={journalId} />
      </Layout>
    ),
  };
}

export default action;
