import React from 'react';
import AddJournal from './AddJournal';
import Layout from '../../../components/Layout';


async function action() {
  const breadcrumbs = [
    { label: 'Журналы', href: '/journals' },
    { label: 'Создать журнал' },
  ];

  return {
    title: 'Создать журнал',
    component: (
      <Layout breadcrumbs={breadcrumbs}>
        <AddJournal />
      </Layout>

    ),
  };
}

export default action;
