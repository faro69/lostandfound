/* eslint-disable camelcase */
// @flow
import React from 'react';
import { connect } from 'react-redux';
import { createJournal } from '../../../redux/modules/journals';
import JournalForm from '../JournalForm';

type Props = {
  createJournal: Function,
};

class AddJournal extends React.Component<Props> {
  onSubmit = ({ date_print, ...rest }) => {
    const [month_print, day_print, year_print] = date_print.split('-');

    this.props.createJournal({
      ...rest,
      month_print,
      day_print,
      year_print,
    });
  }

  render() {
    const formSettings = {
      form: 'addJournal',
      onSubmit: this.onSubmit,
      initialValues: {
      },
      closeHref: '/journals',
    };
    return (
      <JournalForm {...formSettings} />
    );
  }
}

const mapDispatch = { createJournal };
export default connect(null, mapDispatch)(AddJournal);
