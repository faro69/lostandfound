// @flow

import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import Link from '../../components/Link';
import Button from '../../components/Button';
import { generateJournalTemplate } from '../../redux/modules/journals';
import type { JournalsState } from '../../redux/types/journals';

type Props = {
  journals: JournalsState,
  generateJournalTemplate: Function,
  loading: boolean,
}

class Journals extends React.Component<Props> {
  generateJournalTemplate = id => (e) => {
    e.preventDefault();
    this.props.generateJournalTemplate(id);
  }

  render() {
    const { journals: { items }, loading } = this.props;
    return (
      <div className="journals">
        {
          !items.length && <h3>У Вас нет ни одного журнала.</h3>
        }
        <p>
          <Button primary href="/journals/add">Создать журнал</Button>
        </p>
        <ul className="list-group">
          {
            loading ? <p>Загрузка...</p> :
            items.map(journal =>
              (
                <li className="list-group-item d-flex justify-content-between align-items-center" key={journal.id}>
                  <Link
                    href={`/journals/${journal.id}`}
                  >
                    {journal.title_rus} / {journal.title_eng}
                  </Link>
                  {
                    journal.doi_template_url ?
                      (
                        <div>
                          <a
                            href={journal.doi_template_url}
                            target="_blank"
                          >
                            Шаблон журнала
                          </a>
                          &nbsp;
                          <Link
                            href={`/journals/${journal.id}`}
                            onClick={this.generateJournalTemplate(journal.id)}
                          >
                            (создать шаблон заново)
                          </Link>
                        </div>
                      )
                       :
                        <Link
                          href={`/journals/${journal.id}`}
                          onClick={this.generateJournalTemplate(journal.id)}
                        >
                        Создать шаблон
                        </Link>
                  }
                </li>
              ))
          }
        </ul>
      </div>
    );
  }
}

const mapState = ({ journals }) => ({ journals, loading: journals.loading });
const mapDispatch = { generateJournalTemplate };
export default connect(mapState, mapDispatch)(Journals);
