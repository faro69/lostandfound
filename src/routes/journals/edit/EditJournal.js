// @flow
import React from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import { updateJournal } from '../../../redux/modules/journals';
import JournalForm from '../JournalForm';
import type { Journal } from '../../../redux/types/journals';

type Props = {
  journalId: number,
  journal: Journal,
  updateJournal: Function,
};

class EditJournal extends React.Component<Props> {
  onSubmit = (data) => {
    this.props.updateJournal(data);
  }

  render() {
    const { journal } = this.props;
    const formSettings = {
      form: 'editJournal',
      onSubmit: this.onSubmit,
      initialValues: {
        ...journal,
        date_print: moment(`${journal.day_print}-${journal.month_print}-${journal.year_print}`).format('DD-MM-YYYY'),
      },
      closeHref: `/journals/${journal.id}`,
    };
    return (
      <JournalForm {...formSettings} edit />
    );
  }
}

const mapState = ({ journals }, { journalId }) => ({ journal: journals.byId[journalId] });
const mapDispatch = { updateJournal };
export default connect(mapState, mapDispatch)(EditJournal);
