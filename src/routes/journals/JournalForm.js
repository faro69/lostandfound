// @flow
import React, { Fragment } from 'react';
import { reduxForm } from 'redux-form';
import Input from '../../components/Input/index';
import Button from '../../components/Button/index';
import Datepicker from '../../components/Datepicker';

type Props = {
  handleSubmit: Function,
  edit: boolean,
  closeHref: string,
}

const articlesMultilangFields = [
  { name: 'title', label: 'Название журнала', required: true },
  { name: 'abbr', label: 'Аббревиатура', required: true },
];

class JournalForm extends React.Component<Props> {
  render() {
    const { handleSubmit, edit, closeHref } = this.props;
    return (
      <form onSubmit={handleSubmit}>
        <h1>{edit ? 'Изменить' : 'Создать'} журнал</h1>
        <div className="row">
          {
            articlesMultilangFields.map(langField =>
              (
                <Fragment key={langField.name}>
                  <div className="col-md-6">
                    <Input name={`${langField.name}_rus`} label={langField.label} required={langField.required} />
                  </div>
                  <div className="col-md-6">
                    <Input name={`${langField.name}_eng`} label={`${langField.label} на английском`} required={langField.required} />
                  </div>
                </Fragment>
              ))
          }
          <div className="col-lg-6">
            <Input name="issn" label="ISSN" required />
          </div>
          <div className="col-lg-6">
            <Datepicker name="year" label="Год издания" dateFormat="YYYY" required />
          </div>
          <div className="col-lg-6">
            <Datepicker name="date_print" label="Дата печати" required />
          </div>
          <div className="col-lg-6">
            <Input name="number_rel" label="Номер выпуска" required isNumber />
          </div>
          <div className="col-lg-6">
            <Input name="number_chast" label="Номер части" required isNumber />
          </div>
          <div className="col-lg-6">
            <Input name="release_name" label="Название выпуска" required />
          </div>
          <div className="col-lg-6">
            <Input name="pages" label="Страницы" required />
          </div>
          <div className="col-lg-6">
            <Input name="tom" label="Том" required />
          </div>
        </div>
        <div className="btn-toolbar">
          <div className="btn-group mr-2">
            <Button type="submit" success>
              {edit ? 'Изменить' : 'Создать'} журнал
            </Button>
          </div>
          <div className="btn-group">
            <Button danger href={closeHref}>Закрыть</Button>
          </div>
        </div>
      </form>
    );
  }
}

export default reduxForm()(JournalForm);
