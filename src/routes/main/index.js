import React from 'react';
import Link from '../../components/Link';
import Layout from '../../components/Layout';
import authorIcon from './author_icon.svg';
import journalIcon from './journal_icon.svg';
import './style.css';

async function action() {
  return {
    title: 'Главная страница',
    component: (
      <Layout>
        <div className="container">
          <div className="row">
            <div className="col-md-6">
              <Link href="/journals" className="bImageLink">
                <img src={journalIcon} alt="Журналы" className="icon" />
                <h4>Журналы</h4>
              </Link>
            </div>
            <div className="col-md-6" >
              <Link href="/authors" className="bImageLink">
                <img src={authorIcon} alt="Авторы" className="icon" />
                <h4>Авторы</h4>
              </Link>
            </div>
          </div>
        </div>
      </Layout>
    ),
  };
}

export default action;
