import Input from '../../components/Input';
import Datepicker from '../../components/Datepicker';

export const LINK_TYPES = {
  BOOK: 1,
  REDACTED_BOOK: 2,
  CONF_ARTICLE: 3,
  JOURNAL_ARTICLE: 4,
  DISSERTATION: 5,
  ELECTRONIC_RESOURCE: 6,
};

export const linksLanguageOptions = [
  { label: 'Русский', value: 'russian' },
  { label: 'Английский', value: 'english' },
];


export const linksOptions = [
  { label: 'Книга', value: LINK_TYPES.BOOK },
  { label: 'Книга под редакцией', value: LINK_TYPES.REDACTED_BOOK },
  { label: 'Статья из конференции', value: LINK_TYPES.CONF_ARTICLE },
  { label: 'Статья из журнала', value: LINK_TYPES.JOURNAL_ARTICLE },
  { label: 'Диссертация', value: LINK_TYPES.DISSERTATION },
  { label: 'Электронный ресурс', value: LINK_TYPES.ELECTRONIC_RESOURCE },
];

export const linksNamesByType = {
  [LINK_TYPES.BOOK]: ['link_type', 'article_id', 'language',
    'title', 'authors',
    'additional_data', 'publishing_address',
    'publishing', 'tom', 'year', 'pages'],

  [LINK_TYPES.REDACTED_BOOK]: ['link_type', 'article_id', 'language',
    'title', 'editors',
    'additional_data', 'publishing_address',
    'publishing', 'tom', 'year', 'pages'],

  [LINK_TYPES.CONF_ARTICLE]: ['link_type', 'article_id', 'language',
    'title', 'authors', 'additional_article_data',
    'conf_name', 'conf_additional', 'series',
    'conf_place', 'conf_date', 'publishing_address',
    'publishing', 'tom', 'year', 'release', 'pages'],

  [LINK_TYPES.JOURNAL_ARTICLE]: ['link_type', 'article_id', 'language',
    'title', 'authors', 'journal_title',
    'additional_journal_data', 'article_title',
    'additional_article_data', 'tom', 'year', 'pages'],

  [LINK_TYPES.DISSERTATION]: ['link_type', 'article_id', 'language',
    'title', 'authors', 'degree',
    'publishing_address', 'publishing', 'year', 'pages'],
  [LINK_TYPES.ELECTRONIC_RESOURCE]: ['link_type', 'article_id', 'language',
    'title', 'additional_data',
    'reference', 'application_date'],
};

export const fieldLabelsByName = {
  title: 'Название',
  authors: 'Авторы',
  additional_data: 'Доп.сведения',
  publishing_address: 'Место издательства',
  publishing: 'Издательство',
  tom: 'Том',
  year: 'Год издания',
  pages: 'Страницы',
  editors: 'Редакторы',
  article_title: 'Название статьи',
  additional_article_data: 'Доп.сведения статьи',
  conf_name: 'Название конференции',
  conf_additional: 'Доп.сведения конференции',
  series: 'Серия',
  conf_place: 'Место проведения конференции',
  conf_date: 'Дата проведения конференции',
  release: 'Выпуск',
  journal_title: 'Название журнала',
  additional_journal_data: 'Доп.сведения журнала',
  degree: 'Ученая степень',
  reference: 'Ссылка',
  application_date: 'Дата обращения',
};

export const formFields = {
  [LINK_TYPES.BOOK]: [
    { component: Input, name: 'title', required: true },
    { component: Input, name: 'authors', required: true },
    { component: Input, name: 'additional_data', required: false },
    { component: Input, name: 'publishing_address', required: true },
    { component: Input, name: 'publishing', required: true },
    { component: Input, name: 'tom', required: true },
    {
      component: Datepicker, name: 'year', required: true, dateFormat: 'YYYY',
    },
    { component: Input, name: 'pages', required: true },
  ],

  [LINK_TYPES.REDACTED_BOOK]: [
    { component: Input, name: 'title', required: true },
    { component: Input, name: 'editors', required: true },
    { component: Input, name: 'additional_data', required: false },
    { component: Input, name: 'publishing_address', required: true },
    { component: Input, name: 'publishing', required: true },
    {
      component: Input, name: 'tom', required: true, isNumber: true,
    },
    {
      component: Datepicker, name: 'year', required: true, dateFormat: 'YYYY',
    },
    { component: Input, name: 'pages', required: true },
  ],

  [LINK_TYPES.CONF_ARTICLE]: [
    { component: Input, name: 'title', required: true },
    { component: Input, name: 'authors', required: true },
    { component: Input, name: 'article_title', required: true },
    { component: Input, name: 'additional_article_data', required: false },
    { component: Input, name: 'conf_name', required: true },
    { component: Input, name: 'conf_additional', required: false },
    { component: Input, name: 'series', required: true },
    { component: Input, name: 'conf_place', required: false },
    { component: Datepicker, name: 'conf_date', required: false },
    { component: Input, name: 'publishing_address', required: true },
    { component: Input, name: 'publishing', required: true },
    {
      component: Input, name: 'tom', required: true, isNumber: true,
    },
    {
      component: Datepicker, name: 'year', required: true, dateFormat: 'YYYY',
    },
    {
      component: Input, name: 'release', required: true, dateFormat: 'YYYY',
    },
    { component: Input, name: 'pages', required: true },
  ],

  [LINK_TYPES.JOURNAL_ARTICLE]: [
    { component: Input, name: 'title', required: true },
    { component: Input, name: 'authors', required: true },
    { component: Input, name: 'journal_title', required: true },
    { component: Input, name: 'additional_journal_data', required: false },
    { component: Input, name: 'article_title', required: true },
    { component: Input, name: 'additional_article_data', required: false },
    {
      component: Input, name: 'tom', required: true, isNumber: true,
    },
    {
      component: Datepicker, name: 'year', required: true, dateFormat: 'YYYY',
    },
    { component: Input, name: 'pages', required: true },
  ],

  [LINK_TYPES.DISSERTATION]: [
    { component: Input, name: 'title', required: true },
    { component: Input, name: 'authors', required: true },
    { component: Input, name: 'degree', required: true },
    { component: Input, name: 'publishing_address', required: true },
    { component: Input, name: 'publishing', required: true },
    {
      component: Datepicker, name: 'year', required: true, dateFormat: 'YYYY',
    },
    { component: Input, name: 'pages', required: true },
  ],
  [LINK_TYPES.ELECTRONIC_RESOURCE]: [
    { component: Input, name: 'title', required: true },
    { component: Input, name: 'additional_data', required: false },
    { component: Input, name: 'reference', required: true },
    { component: Input, name: 'application_date', required: true },
  ],
};

