// @flow

import React from 'react';
import { connect } from 'react-redux';
import Link from '../../components/Link';
import Button from '../../components/Button';
import type { Link as LinkType } from '../../redux/types/links';

type Props = {
  links: Array<LinkType>,
  articleId: number,
  journalId: number,
}

class Links extends React.Component<Props> {
  render() {
    const {
      links = [], journalId, articleId,
    } = this.props;
    return (
      <div className="journals">
        <h1>Ссылки</h1>
        {
          !links.length && <h5>Нет ни одной ссылки.</h5>
        }
        <p>
          <Button primary href={`/journals/${journalId}/articles/${articleId}/links/add`}>
            Добавить ссылку
          </Button>
        </p>
        <ul className="list-group">
          {
            links.map(link =>
              (
                <li
                  className="list-group-item d-flex justify-content-between align-items-center"
                  key={link.id}
                >
                  <Link
                    href={`/journals/${journalId}/articles/${articleId}/links/${link.id}`}
                  >
                    {link.title}
                  </Link>
                </li>
              ))
          }
        </ul>
      </div>
    );
  }
}

const mapState = ({ links }, { articleId }) => ({
  links: links.byArticle[articleId],
});
export default connect(mapState)(Links);
