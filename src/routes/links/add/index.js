import React from 'react';
import isEmpty from 'just-is-empty';
import Layout from '../../../components/Layout';
import { fetchJournal } from '../../../redux/modules/journals';
import { fetchArticle } from '../../../redux/modules/articles';
import { fetchArticleLinks } from '../../../redux/modules/links';
import AddLink from './AddLink';

async function action({ store: { getState, dispatch }, params: { journalId, articleId } }) {
  const { links } = getState();
  let { articles, journals } = getState();

  await Promise.all([
    isEmpty(journals.byId[journalId]) && dispatch(fetchJournal(journalId)),
    isEmpty(articles.byId[articleId]) && dispatch(fetchArticle(articleId)),
    isEmpty(links.byArticle[articleId]) && dispatch(fetchArticleLinks(articleId)),
  ]);
  ({
    articles,
    journals,
  } = getState());
  const journal = journals.byId[journalId];
  const article = articles.byId[articleId];

  if (isEmpty(journal) || isEmpty(article) || article.journal_id !== journal.id) {
    return { redirect: '/404' };
  }

  const breadcrumbs = [
    { label: 'Журналы', href: '/journals' },
    {
      label: `${journal.title_rus} / ${journal.title_eng}`,
      href: `/journals/${journalId}`,
    },
    { label: 'Статьи', href: `/journals/${journalId}/articles` },
    {
      label: `${article.title_rus} / ${article.title_eng}`,
      href: `/journals/${journalId}/articles/${articleId}`,
    },
    { label: 'Ссылки', href: `/journals/${journalId}/articles/${articleId}/links` },
    { label: 'Добавить ссылку' },
  ];

  return {
    title: `Добавить ссылку для статьи ${article.title_rus} / ${article.title_eng}`,
    component: (
      <Layout breadcrumbs={breadcrumbs}>
        <AddLink
          journalId={journalId}
          articleId={articleId}
          closeHref={`/journals/${journalId}/articles/${articleId}/links`}
        />
      </Layout>
    ),
  };
}

export default action;
