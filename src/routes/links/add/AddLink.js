/* eslint-disable camelcase */
// @flow
import React from 'react';
import { connect } from 'react-redux';
import pick from 'just-pick';
import { createLink } from '../../../redux/modules/links';
import LinkForm from '../LinkForm';
import { formFields, LINK_TYPES, linksNamesByType } from '../linksData';

type Props = {
  createLink: Function,
  journalId: number,
  articleId: number,
  closeHref: string,
};

class AddLink extends React.Component<Props> {
  onSubmit = (data) => {
    const { journalId, articleId } = this.props;
    this.props.createLink({
      data: { ...pick(data, linksNamesByType[data.link_type]) },
      journalId,
      articleId,
    });
  }

  render() {
    const { articleId, closeHref } = this.props;
    const formSettings = {
      form: 'addLink',
      onSubmit: this.onSubmit,
      initialValues: {
        article_id: articleId,
        link_type: LINK_TYPES.BOOK,
        language: 'russian',
      },
      closeHref,
    };
    return (
      <LinkForm {...formSettings} />
    );
  }
}

const mapDispatch = { createLink };
export default connect(null, mapDispatch)(AddLink);
