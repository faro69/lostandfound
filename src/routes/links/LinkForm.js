// @flow
import React from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { formValueSelector, reduxForm } from 'redux-form';
import Button from '../../components/Button/index';
import { fieldLabelsByName, formFields, linksLanguageOptions, linksOptions } from './linksData';
import Select from '../../components/Select';

type Props = {
  handleSubmit: Function,
  edit: boolean,
  closeHref: string,
  selectedLinkType: number,
}


class LinkForm extends React.Component<Props> {
  render() {
    const {
      handleSubmit, edit, closeHref, selectedLinkType,
    } = this.props;

    const fields = formFields[selectedLinkType] || [];
    return (
      <form onSubmit={handleSubmit}>
        <h1>{edit ? 'Изменить' : 'Добавить'} ссылку</h1>
        <div className="row">
          {
            !edit &&
            <div className="col-lg-6">
              <Select name="link_type" label="Тип ссылки" options={linksOptions} required />
            </div>
          }
          <div className="col-lg-6">
            <Select name="language" label="Язык ссылки" options={linksLanguageOptions} required />
          </div>
          {
            fields.map(({ component, name, ...props }) => (
              <div className="col-lg-6" key={name}>
                {
                  React.createElement(component, { name, label: fieldLabelsByName[name], ...props })
                }
              </div>))
          }
        </div>
        <div className="btn-toolbar">
          <div className="btn-group mr-2">
            <Button type="submit" success>
              {edit ? 'Изменить' : 'Добавить'} ссылку
            </Button>
          </div>
          <div className="btn-group">
            <Button danger href={closeHref}>Закрыть</Button>
          </div>
        </div>
      </form>
    );
  }
}

const mapState = (state, { form }) => ({
  selectedLinkType: formValueSelector(form)(state, 'link_type'),
});


export default compose(reduxForm(), connect(mapState))(LinkForm);
