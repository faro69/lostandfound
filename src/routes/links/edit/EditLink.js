/* eslint-disable camelcase */
// @flow
import React from 'react';
import { connect } from 'react-redux';
import pick from 'just-pick';
import { createLink, updateLink } from '../../../redux/modules/links';
import LinkForm from '../LinkForm';
import { formFields, LINK_TYPES, linksNamesByType } from '../linksData';
import type { Link } from '../../../redux/types/links';

type Props = {
  updateLink: Function,
  journalId: number,
  articleId: number,
  closeHref: string,
  link: Link,
};

class EditLink extends React.Component<Props> {
  onSubmit = ({ id, ...body }) => {
    const { journalId, articleId } = this.props;
    this.props.updateLink({
      id,
      body,
      journalId,
      articleId,
    });
  }

  render() {
    const { link, closeHref } = this.props;
    const formSettings = {
      form: 'editLink',
      onSubmit: this.onSubmit,
      initialValues: {
        ...link,
      },
      edit: true,
      closeHref,
    };
    return (
      <LinkForm {...formSettings} />
    );
  }
}

const mapState = ({ links }, { linkId }) => ({ link: links.byId[linkId] });
const mapDispatch = { updateLink };

export default connect(mapState, mapDispatch)(EditLink);
