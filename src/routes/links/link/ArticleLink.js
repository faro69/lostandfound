// @flow

import React from 'react';
import { connect } from 'react-redux';
import type { Link as LinkType } from '../../../redux/types/links';
import { deleteLink } from '../../../redux/modules/links';
import ArticleLinkCard from '../../../components/ArticleLink';

type Props = {
  journalId: number,
  articleId: number,
  link: LinkType,
  deleteFunc: Function,
}

class ArticleLink extends React.Component<Props> {
  render() {
    const {
      link, deleteFunc, journalId, articleId,
    } = this.props;
    return (
      <ArticleLinkCard
        link={link}
        deleteFunc={deleteFunc}
        journalId={journalId}
        articleId={articleId}
        linkId={link.id}
      />
    );
  }
}

const mapState = ({ links }, { linkId }) => ({ link: links.byId[linkId] });
export default connect(mapState, { deleteFunc: deleteLink })(ArticleLink);
