import React from 'react';
import isEmpty from 'just-is-empty';
import Layout from '../../../components/Layout';
import { fetchJournal } from '../../../redux/modules/journals';
import { fetchArticle } from '../../../redux/modules/articles';
import { fetchLink } from '../../../redux/modules/links';
import ArticleLink from './ArticleLink';

async function action({ store: { getState, dispatch }, params: { journalId, articleId, linkId } }) {
  let { articles, journals, links } = getState();

  await Promise.all([
    isEmpty(journals.byId[journalId]) && dispatch(fetchJournal(journalId)),
    isEmpty(articles.byId[articleId]) && dispatch(fetchArticle(articleId)),
    isEmpty(links.byId[linkId]) && dispatch(fetchLink(linkId)),
  ]);
  ({
    articles,
    journals,
    links,
  } = getState());
  const journal = journals.byId[journalId];
  const article = articles.byId[articleId];
  const link = links.byId[linkId];

  if (isEmpty(journal) ||
    isEmpty(article) ||
    article.journal_id !== journal.id ||
    link.article_id !== article.id) {
    return { redirect: '/404' };
  }

  const breadcrumbs = [
    { label: 'Журналы', href: '/journals' },
    {
      label: `${journal.title_rus} / ${journal.title_eng}`,
      href: `/journals/${journalId}`,
    },
    { label: 'Статьи', href: `/journals/${journalId}/articles` },
    {
      label: `${article.title_rus} / ${article.title_eng}`,
      href: `/journals/${journalId}/articles/${articleId}`,
    },
    { label: 'Ссылки', href: `/journals/${journalId}/articles/${articleId}/links` },
    { label: link.title },
  ];

  return {
    title: `Ссылка ${link.title}`,
    component: (
      <Layout breadcrumbs={breadcrumbs}>
        <ArticleLink
          journalId={journalId}
          articleId={articleId}
          linkId={linkId}
          closeHref={`/journals/${journalId}/articles/${articleId}/links`}
        />
      </Layout>
    ),
  };
}

export default action;
