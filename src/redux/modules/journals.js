// @flow

import { createAction, createReducer } from 'redux-act';
import omit from 'just-omit';
import type { Journal, JournalsState } from '../types/journals';

export const fetchJournalsStart = createAction('JOURNALS/START/FETCH journals list');
export const fetchJournalsSuccess = createAction('JOURNALS/SUCCESS/FETCH journals list');
export const fetchJournalsFail = createAction('JOURNALS/FAIL/FETCH journals list');

export const fetchJournalStart = createAction('JOURNALS/START/FETCH journal item');
export const fetchJournalSuccess = createAction('JOURNALS/SUCCESS/FETCH journal item');
export const fetchJournalFail = createAction('JOURNALS/FAIL/FETCH journal item');

export const createJournalStart = createAction('JOURNALS/START/create journal item');
export const createJournalSuccess = createAction('JOURNALS/SUCCESS/create journal item');
export const createJournalFail = createAction('JOURNALS/FAIL/create journal item');

export const updateJournalStart = createAction('JOURNALS/START/update journal item');
export const updateJournalSuccess = createAction('JOURNALS/SUCCESS/update journal item');
export const updateJournalFail = createAction('JOURNALS/FAIL/update journal item');

export const deleteJournalStart = createAction('JOURNALS/START/delete journal item');
export const deleteJournalSuccess = createAction('JOURNALS/SUCCESS/delete journal item');
export const deleteJournalFail = createAction('JOURNALS/FAIL/delete journal item');

export const generateJournalTemplateStart = createAction('JOURNALS/START/generate journal template');
export const generateJournalTemplateSuccess = createAction('JOURNALS/SUCCESS/generate journal template');
export const generateJournalTemplateFail = createAction('JOURNALS/FAIL/generate journal template');

const initialState: JournalsState = {
  loading: false,
  error: null,
  items: [],
  byId: {},
};

export default createReducer(
  {
    [fetchJournalsStart.toString()]: state => ({ ...state, loading: true, error: null }),
    [fetchJournalsSuccess.toString()]: (state, { result }) => ({ ...state, items: result, loading: false }),
    [fetchJournalsFail.toString()]: (state, error) => ({ ...state, error, loading: false }),

    [fetchJournalStart.toString()]: state => ({ ...state, loading: true, error: null }),
    [fetchJournalSuccess.toString()]: (state, { result, id }) => ({ ...state, byId: { ...state.byId, [id]: result }, loading: false }),
    [fetchJournalFail.toString()]: (state, error) => ({ ...state, error, loading: false }),

    [createJournalStart.toString()]: state => ({ ...state, loading: true, error: null }),
    [createJournalSuccess.toString()]: (state, { result }) => ({
      ...state,
      items: [...state.items, result],
      byId: { ...state.byId, [result.id]: result },
      loading: false,
    }),
    [createJournalFail.toString()]: (state, error) => ({ ...state, error, loading: false }),

    [updateJournalStart.toString()]: state => ({ ...state, loading: true, error: null }),
    [updateJournalSuccess.toString()]: (state, { result, journalId }) => {
      const i = state.items.findIndex(j => j.id === journalId);
      return {
        ...state,
        loading: false,
        items: [...state.items.slice(0, i), result, ...state.items.slice(i + 1)],
        byId: { ...state.byId, [journalId]: result },
      };
    },
    [updateJournalFail.toString()]: (state, error) => ({ ...state, error, loading: false }),

    [deleteJournalStart.toString()]: state => ({ ...state, loading: true, error: null }),
    [deleteJournalSuccess.toString()]: (state, { journalId }) => ({
      ...state,
      loading: false,
      items: [],
      byId: { ...state.byId, [journalId]: {} },
    }),
    [deleteJournalFail.toString()]: (state, error) => ({ ...state, error, loading: false }),

    [generateJournalTemplateStart.toString()]: state => ({ ...state, loading: true, error: null }),
    [generateJournalTemplateSuccess.toString()]: (state, { result, journalId }) => {
      const i = state.items.findIndex(j => j.id === journalId);
      return {
        ...state,
        loading: false,
        items: [...state.items.slice(0, i), result, ...state.items.slice(i + 1)],
        byId: { ...state.byId, [journalId]: result },
      };
    },
    [generateJournalTemplateFail.toString()]: (state, error) => ({ ...state, error, loading: false }),
  },
  initialState,
);

export const fetchJournals = () => ({
  types: [fetchJournalsStart, fetchJournalsSuccess, fetchJournalsFail],
  promise: ({ fetch }) => fetch('/api/journals', { method: 'get' }),
});

export const fetchJournal = (id: number) => ({
  types: [fetchJournalStart, fetchJournalSuccess, fetchJournalFail],
  promise: ({ fetch }) => fetch(`/api/journals/${id}`, { method: 'get' }),
  id,
});

export const createJournal = (data: Journal) => ({
  types: [createJournalStart, createJournalSuccess, createJournalFail],
  promise: ({ fetch }) => fetch('/api/journals', { body: JSON.stringify(data) }),
  redirect: '/journals',
});

export const updateJournal = ({ id, ...body }: Journal) => ({
  types: [updateJournalStart, updateJournalSuccess, updateJournalFail],
  promise: ({ fetch }) => fetch(`/api/journals/${id}`, { body: JSON.stringify(omit(body, ['createdAt', 'updatedAt'])), method: 'PUT' }),
  redirect: `/journals/${id}`,
  journalId: id,
});

export const deleteJournal = (id: number) => ({
  types: [deleteJournalStart, deleteJournalSuccess, deleteJournalFail],
  promise: ({ fetch }) => fetch(`/api/journals/${id}`, { body: {}, method: 'DELETE' }),
  redirect: '/journals',
  journalId: id,
});

export const generateJournalTemplate = (id: number) => ({
  types: [generateJournalTemplateStart, generateJournalTemplateSuccess, generateJournalTemplateFail],
  promise: ({ fetch }) => fetch(`/api/journals/${id}/template`, { noHeaders: true }),
  journalId: id,
});

