import { createAction, createReducer } from 'redux-act';

export const userLoginStart = createAction('USERS/START/LOGIN');
export const userLoginSuccess = createAction('USERS/SUCCESS/LOGIN');
export const userLoginFail = createAction('USERS/FAIL/LOGIN');

export const userLogoutStart = createAction('USERS/START/LOGOUT');
export const userLogoutSuccess = createAction('ERS/SUCCESS/LOGOUT');
export const userLogoutFail = createAction('USERS/FAIL/LOGOUT');

const initialState = {
  token: null,
  loading: false,
};

export default createReducer({
  [userLoginStart.toString()]: state => ({ ...state, loading: true, error: null }),
  [userLoginSuccess.toString()]: (state, { result }) => ({ ...state, token: result, loading: false }),
  [userLoginFail.toString()]: (state, error) => ({ ...state, error, loading: false }),
  [userLogoutStart.toString()]: state => ({ ...state, loading: true, error: null }),
  [userLogoutSuccess.toString()]: state => ({ ...state, token: null, loading: false }),
  [userLogoutFail.toString()]: (state, error) => ({ ...state, error, loading: false }),
}, initialState);


export const userLogin = () => ({
  types: [userLoginStart, userLoginSuccess, userLoginFail],
  promise: ({ fetch }) => fetch('/api/login'),
});


export const userLogout = () => ({
  types: [userLogoutStart, userLogoutSuccess, userLogoutFail],
  promise: ({ fetch }) => fetch('/api/logout', { method: 'delete' }),
});
