// @flow

import { createAction, createReducer } from 'redux-act';
import { stringify } from 'query-string';
import omit from 'just-omit';
import type { ArticlesState, Article } from '../types/articles';

export const fetchArticlesStart = createAction('ARTICLES/START/FETCH articles list');
export const fetchArticlesSuccess = createAction('ARTICLES/SUCCESS/FETCH articles list');
export const fetchArticlesFail = createAction('ARTICLES/FAIL/FETCH articles list');

export const createArticleStart = createAction('ARTICLES/START/create article item');
export const createArticleSuccess = createAction('ARTICLES/SUCCESS/create article item');
export const createArticleFail = createAction('ARTICLES/FAIL/create article item');

export const fetchArticleStart = createAction('ARTICLES/START/FETCH article item');
export const fetchArticleSuccess = createAction('ARTICLES/SUCCESS/FETCH article item');
export const fetchArticleFail = createAction('ARTICLES/FAIL/FETCH article item');

export const updateArticleStart = createAction('ARTICLES/START/update article item');
export const updateArticleSuccess = createAction('ARTICLES/SUCCESS/update article item');
export const updateArticleFail = createAction('ARTICLES/FAIL/update article item');

export const deleteArticleStart = createAction('ARTICLES/START/delete article item');
export const deleteArticleSuccess = createAction('ARTICLES/SUCCESS/delete article item');
export const deleteArticleFail = createAction('ARTICLES/FAIL/delete article item');

const initialState: ArticlesState = {
  loading: false,
  error: null,
  byJournal: {},
  byId: {},
};

export default createReducer(
  {
    [fetchArticlesStart.toString()]: state => ({ ...state, loading: true, error: null }),
    [fetchArticlesSuccess.toString()]: (state, { result, journalId }) =>
      ({ ...state, loading: false, byJournal: { ...state.byJournal, [journalId]: result } }),
    [fetchArticlesFail.toString()]: (state, error) => ({ ...state, error, loading: false }),

    [createArticleStart.toString()]: state => ({ ...state, loading: true, error: null }),
    [createArticleSuccess.toString()]: (state, { result, journalId }) =>
      ({
        ...state,
        loading: false,
        byId: { ...state.byId, [result.id]: result },
        byJournal: {
          ...state.byJournal,
          [journalId]: [...(state.byJournal[journalId] || {}), result],
        },
      }),
    [createArticleFail.toString()]: (state, error) => ({ ...state, error, loading: false }),

    [fetchArticleStart.toString()]: state => ({ ...state, loading: true, error: null }),
    [fetchArticleSuccess.toString()]: (state, { result, articleId }) =>
      ({
        ...state,
        loading: false,
        byId: { ...state.byId, [articleId]: result },
      }),
    [fetchArticleFail.toString()]: (state, error) => ({ ...state, error, loading: false }),

    [updateArticleStart.toString()]: state => ({ ...state, loading: true, error: null }),
    [updateArticleSuccess.toString()]: (state, { result, journalId, articleId }) =>
      ({
        ...state,
        loading: false,
        byId: { ...state.byId, [articleId]: result },
        byJournal: {
          ...state.byJournal,
          [journalId]: [],
        },
      }),
    [updateArticleFail.toString()]: (state, error) => ({ ...state, error, loading: false }),

    [deleteArticleStart.toString()]: state => ({ ...state, loading: true, error: null }),
    [deleteArticleSuccess.toString()]: (state, { journalId, articleId }) =>
      ({
        ...state,
        loading: false,
        byId: { ...state.byId, [articleId]: {} },
        byJournal: {
          ...state.byJournal,
          [journalId]: [],
        },
      }),
    [deleteArticleFail.toString()]: (state, error) => ({ ...state, error, loading: false }),
  },
  initialState,
);

export const fetchJournalArticles = (journalId: number) => ({
  types: [fetchArticlesStart, fetchArticlesSuccess, fetchArticlesFail],
  promise: ({ fetch }) => fetch(`/api/articles/?${stringify({ journal_id: journalId })}`, { method: 'get' }),
  journalId,
});

export const createArticle = ({ data, journalId }: { data: Article, journalId: number}) => ({
  types: [createArticleStart, createArticleSuccess, createArticleFail],
  promise: ({ fetch }) => fetch('/api/articles', { body: JSON.stringify(data) }),
  redirect: `/journals/${journalId}/articles`,
  journalId,
});

export const fetchArticle = (articleId: number) => ({
  types: [fetchArticleStart, fetchArticleSuccess, fetchArticleFail],
  promise: ({ fetch }) => fetch(`/api/articles/${articleId}`, { method: 'get' }),
  articleId,
});

export const updateArticle = ({ journalId, id, ...body }: {journalId: number, id: number, body: Article}) => ({
  types: [updateArticleStart, updateArticleSuccess, updateArticleFail],
  promise: ({ fetch }) => fetch(
    `/api/articles/${id}`,
    { body: JSON.stringify(omit(body, ['createdAt', 'updatedAt'])), method: 'PUT' },
  ),
  redirect: `/journals/${journalId}/articles/${id}`,
  articleId: id,
  journalId,
});

export const deleteArticle = ({ id, journalId }: {id: number, journalId: number}) => ({
  types: [deleteArticleStart, deleteArticleSuccess, deleteArticleFail],
  promise: ({ fetch }) => fetch(`/api/articles/${id}`, { body: {}, method: 'DELETE' }),
  redirect: `/journals/${journalId}/articles`,
  articleId: id,
  journalId,
});
