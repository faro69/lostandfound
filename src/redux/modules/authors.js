// @flow

import { createAction, createReducer } from 'redux-act';
import omit from 'just-omit';
import type { Author, AuthorsState } from '../types/authors';

export const fetchAuthorsStart = createAction('AUTHORS/START/FETCH authors list');
export const fetchAuthorsSuccess = createAction('AUTHORS/SUCCESS/FETCH authors list');
export const fetchAuthorsFail = createAction('AUTHORS/FAIL/FETCH authors list');

export const fetchAuthorStart = createAction('AUTHORS/START/FETCH journal item');
export const fetchAuthorSuccess = createAction('AUTHORS/SUCCESS/FETCH journal item');
export const fetchAuthorFail = createAction('AUTHORS/FAIL/FETCH journal item');

export const createAuthorStart = createAction('AUTHORS/START/create journal item');
export const createAuthorSuccess = createAction('AUTHORS/SUCCESS/create journal item');
export const createAuthorFail = createAction('AUTHORS/FAIL/create journal item');

export const updateAuthorStart = createAction('AUTHORS/START/update journal item');
export const updateAuthorSuccess = createAction('AUTHORS/SUCCESS/update journal item');
export const updateAuthorFail = createAction('AUTHORS/FAIL/update journal item');

export const deleteAuthorStart = createAction('AUTHORS/START/delete journal item');
export const deleteAuthorSuccess = createAction('AUTHORS/SUCCESS/delete journal item');
export const deleteAuthorFail = createAction('AUTHORS/FAIL/delete journal item');

const initialState: AuthorsState = {
  loading: false,
  error: null,
  items: [],
  byId: {},
};

export default createReducer(
  {
    [fetchAuthorsStart.toString()]: state => ({ ...state, loading: true, error: null }),
    [fetchAuthorsSuccess.toString()]: (state, { result }) => ({ ...state, items: result, loading: false }),
    [fetchAuthorsFail.toString()]: (state, error) => ({ ...state, error, loading: false }),

    [fetchAuthorStart.toString()]: state => ({ ...state, loading: true, error: null }),
    [fetchAuthorSuccess.toString()]: (state, { result, id }) => ({ ...state, byId: { ...state.byId, [id]: result }, loading: false }),
    [fetchAuthorFail.toString()]: (state, error) => ({ ...state, error, loading: false }),

    [createAuthorStart.toString()]: state => ({ ...state, loading: true, error: null }),
    [createAuthorSuccess.toString()]: (state, { result }) => ({
      ...state,
      items: [...state.items, result],
      byId: { ...state.byId, [result.id]: result },
      loading: false,
    }),
    [createAuthorFail.toString()]: (state, error) => ({ ...state, error, loading: false }),

    [updateAuthorStart.toString()]: state => ({ ...state, loading: true, error: null }),
    [updateAuthorSuccess.toString()]: (state, { result, journalId }) => ({
      ...state,
      loading: false,
      items: [],
      byId: { ...state.byId, [journalId]: result },
    }),
    [updateAuthorFail.toString()]: (state, error) => ({ ...state, error, loading: false }),

    [deleteAuthorStart.toString()]: state => ({ ...state, loading: true, error: null }),
    [deleteAuthorSuccess.toString()]: (state, { journalId }) => ({
      ...state,
      loading: false,
      items: [],
      byId: { ...state.byId, [journalId]: {} },
    }),
    [deleteAuthorFail.toString()]: (state, error) => ({ ...state, error, loading: false }),
  },
  initialState,
);

export const fetchAuthors = () => ({
  types: [fetchAuthorsStart, fetchAuthorsSuccess, fetchAuthorsFail],
  promise: ({ fetch }) => fetch('/api/authors', { method: 'get' }),
});

export const fetchAuthor = (id: number) => ({
  types: [fetchAuthorStart, fetchAuthorSuccess, fetchAuthorFail],
  promise: ({ fetch }) => fetch(`/api/authors/${id}`, { method: 'get' }),
  id,
});

export const createAuthor = (data: Author) => ({
  types: [createAuthorStart, createAuthorSuccess, createAuthorFail],
  promise: ({ fetch }) => fetch('/api/authors', { body: JSON.stringify(data) }),
  redirect: '/authors',
});

export const updateAuthor = ({ id, ...body }: Author) => ({
  types: [updateAuthorStart, updateAuthorSuccess, updateAuthorFail],
  promise: ({ fetch }) => fetch(`/api/authors/${id}`, { body: JSON.stringify(omit(body, ['createdAt', 'updatedAt'])), method: 'PUT' }),
  redirect: `/authors/${id}`,
  journalId: id,
});

export const deleteAuthor = (id: number) => ({
  types: [deleteAuthorStart, deleteAuthorSuccess, deleteAuthorFail],
  promise: ({ fetch }) => fetch(`/api/authors/${id}`, { body: {}, method: 'DELETE' }),
  redirect: '/authors',
  journalId: id,
});

