// @flow

import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import journals from './journals';
import articles from './articles';
import authors from './authors';
import links from './links';

export default combineReducers({
  form: formReducer, journals, articles, authors, links,
});
