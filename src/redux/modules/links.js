// @flow

import { createAction, createReducer } from 'redux-act';
import { stringify } from 'query-string';
import omit from 'just-omit';
import type { Link, LinkState } from '../types/links';

export const fetchLinksStart = createAction('LINKS/START/FETCH links list');
export const fetchLinksSuccess = createAction('LINKS/SUCCESS/FETCH links list');
export const fetchLinksFail = createAction('LINKS/FAIL/FETCH links list');

export const createLinkStart = createAction('LINKS/START/create link item');
export const createLinkSuccess = createAction('LINKS/SUCCESS/create link item');
export const createLinkFail = createAction('LINKS/FAIL/create link item');

export const fetchLinkStart = createAction('LINKS/START/FETCH link item');
export const fetchLinkSuccess = createAction('LINKS/SUCCESS/FETCH link item');
export const fetchLinkFail = createAction('LINKS/FAIL/FETCH link item');

export const updateLinkStart = createAction('LINKS/START/update link item');
export const updateLinkSuccess = createAction('LINKS/SUCCESS/update link item');
export const updateLinkFail = createAction('LINKS/FAIL/update link item');

export const deleteLinkStart = createAction('LINKS/START/delete link item');
export const deleteLinkSuccess = createAction('LINKS/SUCCESS/delete link item');
export const deleteLinkFail = createAction('LINKS/FAIL/delete link item');

const initialState: LinkState = {
  loading: false,
  error: null,
  byArticle: {},
  byId: {},
};

export default createReducer(
  {
    [fetchLinksStart.toString()]: state => ({ ...state, loading: true, error: null }),
    [fetchLinksSuccess.toString()]: (state, { result, articleId }) =>
      ({ ...state, loading: false, byArticle: { ...state.byArticle, [articleId]: result } }),
    [fetchLinksFail.toString()]: (state, error) => ({ ...state, error, loading: false }),

    [createLinkStart.toString()]: state => ({ ...state, loading: true, error: null }),
    [createLinkSuccess.toString()]: (state, { result, articleId }) =>
      ({
        ...state,
        loading: false,
        byId: { ...state.byId, [result.id]: result },
        byArticle: {
          ...state.byArticle,
          [articleId]: [...(state.byArticle[articleId] || {}), result],
        },
      }),
    [createLinkFail.toString()]: (state, error) => ({ ...state, error, loading: false }),

    [fetchLinkStart.toString()]: state => ({ ...state, loading: true, error: null }),
    [fetchLinkSuccess.toString()]: (state, { result, linkId }) =>
      ({
        ...state,
        loading: false,
        byId: { ...state.byId, [linkId]: result },
      }),
    [fetchLinkFail.toString()]: (state, error) => ({ ...state, error, loading: false }),

    [updateLinkStart.toString()]: state => ({ ...state, loading: true, error: null }),
    [updateLinkSuccess.toString()]: (state, { result, articleId, linkId }) =>
      ({
        ...state,
        loading: false,
        byId: { ...state.byId, [linkId]: result },
        byArticle: {
          ...state.byArticle,
          [articleId]: [],
        },
      }),
    [updateLinkFail.toString()]: (state, error) => ({ ...state, error, loading: false }),

    [deleteLinkStart.toString()]: state => ({ ...state, loading: true, error: null }),
    [deleteLinkSuccess.toString()]: (state, { articleId, linkId }) =>
      ({
        ...state,
        loading: false,
        byId: { ...state.byId, [linkId]: {} },
        byArticle: {
          ...state.byArticle,
          [articleId]: [],
        },
      }),
    [deleteLinkFail.toString()]: (state, error) => ({ ...state, error, loading: false }),
  },
  initialState,
);

export const fetchArticleLinks = (articleId: number) => ({
  types: [fetchLinksStart, fetchLinksSuccess, fetchLinksFail],
  promise: ({ fetch }) => fetch(`/api/links/?${stringify({ article_id: articleId })}`, { method: 'get' }),
  articleId,
});

export const createLink = ({ data, articleId, journalId }: { data: Link, articleId: number, journalId: number}) => ({
  types: [createLinkStart, createLinkSuccess, createLinkFail],
  promise: ({ fetch }) => fetch('/api/links', { body: JSON.stringify(data) }),
  redirect: `/journals/${journalId}/articles/${articleId}/links`,
  articleId,
});

export const fetchLink = (linkId: number) => ({
  types: [fetchLinkStart, fetchLinkSuccess, fetchLinkFail],
  promise: ({ fetch }) => fetch(`/api/links/${linkId}`, { method: 'get' }),
  linkId,
});

export const updateLink = ({
  journalId, articleId, id, ...body
}: {journalId: number, articleId: number, id: number, body: Link}) => ({
  types: [updateLinkStart, updateLinkSuccess, updateLinkFail],
  promise: ({ fetch }) => fetch(
    `/api/links/${id}`,
    { body: JSON.stringify(omit(body, ['createdAt', 'updatedAt'])), method: 'PUT' },
  ),
  redirect: `/journals/${journalId}/articles/${articleId}/links/${id}`,
  linkId: id,
  articleId,
});

export const deleteLink = ({ id, journalId, articleId }: {id: number, journalId: number, articleId: number}) => ({
  types: [deleteLinkStart, deleteLinkSuccess, deleteLinkFail],
  promise: ({ fetch }) => fetch(`/api/links/${id}`, { body: {}, method: 'DELETE' }),
  redirect: `/journals/${journalId}/articles/${articleId}/links`,
  linkId: id,
  articleId,
});
