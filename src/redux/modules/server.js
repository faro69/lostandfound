
import { createAction, createReducer } from 'redux-act';

export const sendDataStart = createAction('SERVER/START/send data');
export const sendDataSuccess = createAction('SERVER/SUCCESS/send data');
export const sendDataFail = createAction('SERVER/FAIL/send data');

const initialState = {
  loading: false,
  error: null,
};

export default createReducer(
  {
    [sendDataStart]: state => ({ ...state, loading: true, error: null }),
    [sendDataSuccess]: (state, { result }) => ({
      ...state, links: result, loading: false, error: null,
    }),
    [sendDataFail]: (state, error) => ({ ...state, error, loading: false }),
  },
  initialState,
);

export const sendData = data => ({
  types: [sendDataStart, sendDataSuccess, sendDataFail],
  promise: ({ fetch }) => {
    console.log(data);
    return fetch('/api/articles', { body: JSON.stringify(data) });
  },
});
