function createFetch(fetch, { baseUrl, cookie, apiKey }) {
  // NOTE: Tweak the default options to suite your application needs
  const defaults = {
    method: 'POST', // handy with GraphQL backends
    mode: baseUrl ? 'cors' : 'same-origin',
    credentials: baseUrl ? 'include' : 'same-origin',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      ...(cookie ? { Cookie: cookie } : null),
      ...(apiKey ? { Authorization: apiKey } : null),
    },
  };

  return (url, options) =>
    (url.startsWith('/api')
      ? fetch(`${baseUrl}${url.slice(4)}`, {
        ...defaults,
        ...options,
        headers: {
          ...(options && options.noHeaders ? {} : defaults.headers),
          ...(options && options.headers),
        },
      })
      : fetch(url, options));
}

export default createFetch;
