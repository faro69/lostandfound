// @flow

export type Journal = {
  id: number,
  title_rus: string,
  title_eng: string,
  abbr_rus: string,
  abbr_eng: string,
  issn: string,
  tom: string,
  number_rel: number,
  number_chast: number,
  release_name: string,
  pages: number,
  year: number,
  month_print: number,
  day_print: number,
  year_print: number,
  doi_template_url: string,
}

export type JournalsState = {
  loading: boolean,
  error: any,
  items: Array<Journal>,
  byId: {
    [number]: Journal,
  },
}
