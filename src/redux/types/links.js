// @flow

export type Link = {
  id: number,
  title: string,
  additional_data: string,
  authors: string,
  editors: string,
  publishing: string,
  publishing_address: string,
  tom: number,
  release: number,
  pages: number,
  article_title: number,
  additional_article_data: string,
  conf_place: string,
  conf_date: string,
  series_name: string,
  year: string,
  reference: string,
  application_date: string,
  degree: string,
  is_english: number,
};

export type LinkState = {
  loading: boolean,
  error: any,
  byArticle: {
    [number]: Array<Link>,
  },
  byId: {
    [number]: Link,
  }
}
