// @flow

export type Author = {
  id: number,
  surname_rus: string,
  surname_eng: string,
  name_rus: string,
  name_eng: string,
  patronymic_rus: string,
  patronymic_eng: string,
  job_rus: string,
  job_eng: string,
  city_rus: string,
  city_eng: string,
  email: string,
  addr_rus: string,
  addr_eng: string,
  other_inform_rus: string,
  other_inform_eng: string,
  corresp: string,
  spin: string,
}

export type AuthorsState = {
  loading: boolean,
  error: any,
  items: Array<Author>,
  byId: {
    [number]: Author,
  },
}
