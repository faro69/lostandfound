// @flow

export type Article = {
  id: number,
  title_rus: string,
  title_eng: string,
  jour_section_rus: string,
  jour_section_eng: string,
  type: number,
  pages_first: number,
  pages_last: number,
  annotation_rus: string,
  annotation_eng: string,
  keywords_rus: string,
  keywords_eng: string,
  udc: string,
  bbk: string,
  receipt_date: string,
  recycling_date: string,
  text: string,
  financ_rus: string,
  financ_eng: string,
  doi: string,
  elibrary: string,
  mathnet: string,
  tex_template_url: string,
  elibrary_template_url: string,
  mathnet_template_url: string,
}

export type ArticlesState = {
  loading: boolean,
  error: any,
  byJournal: {
    [number]: Array<Article>,
  },
  byId: {
    [number]: Article,
  },
}
