import cookie from 'react-cookies';
import history from '../../history';
import { userLoginSuccess, userLogoutSuccess } from '../modules/users';

export default function clientMiddleware(helpers) {
  return ({ dispatch, getState }) => next => (action) => {
    if (typeof action === 'function') {
      return action(dispatch, getState);
    }
    const {
      promise, types, redirect, ...rest
    } = action;
    if (!promise) {
      return next(action);
    }

    const [REQUEST, SUCCESS, FAILURE] = types;
    const actionPromise = promise(helpers, dispatch, getState);
    next({ type: REQUEST.getType(), payload: { ...rest } });
    return actionPromise
      .then((response) => {
        if (Array.isArray(response)) {
          return Promise.all(response.map(res => res.json()));
        }
        if (response.status && typeof response.url === 'string') {
          // instance of fetch Response
          if (!response.ok) {
            return Promise.reject(response);
          }
          return response.json();
        }
        return Promise.resolve(response);
      })
      .then(
        (result) => {
          next({ type: SUCCESS.getType(), payload: { ...rest, result } });
          if (SUCCESS.getType() === userLoginSuccess.getType()) {
            const { token } = result;
            next({ type: userLoginSuccess.getType(), payload: { token } });
            cookie.save('token', token);
          }

          if (SUCCESS.getType() === userLogoutSuccess.getType()) {
            next({ type: userLogoutSuccess.getType() });
            cookie.remove('token');
          }

          if (redirect) { history.replace(redirect); }
        },
        (error) => {
          next({ type: FAILURE.getType(), payload: error.statusText, error: true });
          return null;
        },
      ).catch((error) => {
        console.error('MIDDLEWARE ERROR:', error);
        next({ type: FAILURE.getType(), payload: error, error: true });
      });
  };
}
