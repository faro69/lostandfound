/* eslint-disable global-require */
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';
import config from '../config';
import reducer from './modules';
import createFetch from './createFetch';
import clientMiddleware from './clientMiddleware';

const fetch = createFetch(window.fetch, {
  baseUrl: config.apiUrl,
});

const loggerParams = { collapsed: true };
const helpers = { fetch };
const middleware = [clientMiddleware(helpers), thunk, createLogger(loggerParams)];

export default createStore(
  reducer,
  {},
  composeWithDevTools(applyMiddleware(...middleware)),
);
