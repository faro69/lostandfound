var gulp = require('gulp'),
    autoprefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    clean = require('gulp-clean'),
    notify = require("gulp-notify"),
    cssnano = require('gulp-cssnano'),
    stylus = require('gulp-stylus'),
    livereload = require('gulp-livereload'),
    pug = require('gulp-pug'),
    prettify = require('gulp-html-prettify'),
    sourcemaps = require('gulp-sourcemaps'),
    cached = require('gulp-cached'),
    eslint = require('gulp-eslint');

// server
var static = require('node-static');
var opn = require("opn");


//images
gulp.task('image', function() {
    return gulp.src('src/img/**/*.*')
        .pipe(cached('images'))
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{ removeViewBox: false }],
            use: [pngquant()]
        }))
        .pipe(gulp.dest('dist/img'));
});

//clean 
gulp.task('clean', function() {
    return gulp.src('dist', { read: false })
        .pipe(clean());
});

//css
gulp.task('styles', function() {
    return gulp.src('src/css/style.styl')
        .pipe(sourcemaps.init())
        .pipe(stylus())
        .on("error", notify.onError())
        .pipe(autoprefixer("last 15 versions"))
        .pipe(cssnano())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('dist/css/'))
        .pipe(livereload({ start: true }));
});

//js
gulp.task('js', function() {
    return gulp.src('src/js/**/*.js')
        .pipe(cached('js'))
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(gulp.dest('dist/js/'))
        .pipe(livereload({ start: true }));
});

//сторонние библиотеки
gulp.task('vendor', function() {
    return gulp.src('vendor/**/*.*')
        .pipe(cached('vendor'))
        .pipe(gulp.dest('dist/'))
});

//pug building
gulp.task('pug', function() {
    return gulp.src('src/pug/*.pug')
        .pipe(pug({
            pretty: true
        }))
        .on("error", notify.onError())
        .pipe(cached('pug'))
        .pipe(prettify({ indent_char: ' ', indent_size: 2 }))
        .pipe(gulp.dest('dist/'))
        .pipe(livereload({ start: true }));
});

//final building
gulp.task('final', ['styles', 'image', 'js', 'vendor'], function() {
    return gulp.src('src/*.pug')
        .pipe(pug())
        .on("error", notify.onError())
        .pipe(cached('pug'))
        .pipe(prettify({ indent_char: ' ', indent_size: 2 }))
        .pipe(gulp.dest('dist/'))
        .pipe(livereload({ start: true }));
});

gulp.task('server', function() {
    var file = new static.Server('./dist', { cache: 0 });

    opn("http://localhost:8080");
    require('http').createServer(function(request, response) {
        request.addListener('end', function() {
            // 
            // Serve files! 
            // 
            file.serve(request, response);
        }).resume();
    }).listen(8080);
})

gulp.task('default', ['vendor', 'server'], function() {
    livereload.listen();

    gulp.watch("src/**/*.pug", ['pug']);
    gulp.watch("src/css/**/*.styl", ['styles']);
    gulp.watch("src/js/**/*.js", ['js']);
    gulp.watch("src/img/**/*.*", ['image']);
    gulp.watch("vendor/**/*.*", ['vendor']);
});